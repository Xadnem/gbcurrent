/****************************************************************************
 * 
 * MODUL PER A REALITZAR LA CONEXIÓ A LA XARXA WIFI
 * 
 * 
 ****************************************************************************/

#include "WifiConnect.h"

/* Variables globals */
char * ssid;
char * password;
unsigned long previousmillis = 0;
unsigned long interval = 30000;

///////////////////////////////
//
// Conexió a la xarxa WiFi
//
////////////////////////////////////////
//
void conectaWifi()
{
  delay(10);
  Serial.println();
  Serial.print("Conexió a ");
  Serial.println(ssid);
  Serial.print("Amb pwd:  ");
  Serial.println(password);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    vTaskDelay(10);
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("Conectat a WiFi");
  Serial.println("Direcció IP: ");
  Serial.println(WiFi.localIP());
}

///////////////////////
//
// FICA A LA VARIABLE GLOBAL ssid EL NOM DE LA
// XARXA WIFI A LA QUE ES VA A CONNECTAR
//
////////////////////////////////
//
void setSsid(String netid)
{
  int largo = 30;
  ssid = (char*)malloc(largo);
  netid.toCharArray(ssid,largo);
}

///////////////////////
//
//  FICA A LA VARIABLE GLOBAL password EL PASSWORD DE LA
//  XARXA WIFI A LA QUE ES VA A CONNECTAR
//
////////////////////////////////
//
void setPwd(String netpwd)
{
  int largo = 30;
  password = (char*)malloc(largo);
  netpwd.toCharArray(password,largo);
}

///////////////////////////////
//
// Comproba l'estat de connexió a la xarxa wifi i
// es reconnecta donat el cas
//
////////////////////////////////////////
//
void wifiCheck( void * parameter)
{
  while(true)
  {
    unsigned long currentmillis = millis();
    if(WiFi.status() != WL_CONNECTED && (currentmillis - previousmillis >= interval))
    {
      Serial.print("Reconnectant a wifi...");
      WiFi.disconnect();
      WiFi.reconnect();
      previousmillis = currentmillis;
    }
    delay(10);
  }
  vTaskDelete( NULL );
}
