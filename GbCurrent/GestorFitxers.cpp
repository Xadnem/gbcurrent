/****************************************************************************
 * 
 * MODUL PER A REALITZAR OPERACIONS SOBRE FITXERS A UNA TARJETA SD
 * 
 * 
 ****************************************************************************/
#include <ArduinoJson.h>
#include "SD.h"
#include "SPI.h"
#include "GestorFitxers.h"



////////////////////////
//
// COMPRUEBA LA INICIALIZACION CORRECTA
// DE LA TARJETA
//
//////////////////////////////
//
bool compruebaTarjeta(int pin)
{
  if(SD.begin(pin)){
    return true;
  }
 return false;
}

////////////////////////
//
// LEE EL CONTENIDO DEL FICHERO CUYA RUTA
// SE PASA COMO PRIMER ARGUMENTO Y METE SU
// CONTENIDO EN LA ARRAY DE chars PASADA COMO
// SEGUNDO ARGUMENTO CUYO LARGO SE PASA COMO TERCERO
//
//////////////////////////////
//
void leerFichero(char * ruta,char * contenido,int largomax)
{
  File fichero = SD.open(ruta,FILE_READ);
  int puntero = 0;
  if (fichero) {
    // Leer del fichero hasta que no queden bytes
    while (fichero.available()) {
      contenido[puntero] = fichero.read();
      puntero++;
      if(puntero >= largomax)
        break;
    }
    contenido[puntero] = '\0';
    fichero.close();  
  } 
  else {
    Serial.print("error abriendo el fichero." );
    Serial.println(ruta);
   }
}

///////////////////////////////
//
//  DESERIALIZA EL JSON EN LA SD Y METE LOS DATOS PARA 
//  LA CONEXION WIFI Y LA BASE DE DATOS INFLUXDB EN LA
//  LISTA DE CADENAS PASADA COMO ARGUMENTO
//
/////////////////////////////////////////
//
void deserializaJson(String json,const char ** lista)
{  
  // Deserializar el documento JSON
  StaticJsonDocument<1000> doc;
  DeserializationError error = deserializeJson(doc, json);
  // Comprobar si la desearilacion se ha realizado con exito.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  // Meter cada par clave valor en variables de tipo cadena de char
  const char * ssid = doc["SSID"];
  const char * netpwd = doc["NETPWD"];
  const char * host1 = doc["HOST1"];
  const char * host2 = doc["HOST2"];
  const char * host3 = doc["HOST3"];
  const char * host4 = doc["HOST4"];
  const char * port = doc["PORT"];
  
  // Meter las cadenas en la lista de cadenas de char
  lista[0] = ssid;
  lista[1] = netpwd;
  lista[2] = host1;
  lista[3] = host2;
  lista[4] = host3;
  lista[5] = host4;
  lista[6] = port;
}
