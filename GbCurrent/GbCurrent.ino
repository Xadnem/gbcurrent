
#include <Adafruit_ADS1X15.h>
#include <Wire.h>
#include <WiFiUdp.h>
#include "GestorFitxers.h"
#include "WifiConnect.h"
#include "Qua.h"


#define llarg 100
#define largofichero 1000
#define pin 5

Adafruit_ADS1115 ads;
WiFiUDP udp;
float * qua;
int nelements = 0;
char * configuracio;
const float FACTOR = 20; //20A/1V

// Llista de bytes per a la IP del host InfluxDB
byte host[4];
// Port pel què InfluxDB UDP plugin està escoltant
int port;


////////////////////////////
//
//  CREACIÓ DE LES TASQUES , ASSIGNACIÓ 
//  PIN DE LECTURA I INICIALITZACIÓ DE VARIABLES
//
/////////////////////////////////
//
void setup()
{
  Serial.begin(112500);
  Serial.println(__FILE__); 
  delay(1000);
  
  ads.setGain(GAIN_TWO);        // ±2.048V  1 bit = 0.0625mV
  ads.begin();
  
  // Inicialitzar la qua de valors
  qua = (float*)malloc(sizeof(float)*llarg);
  for(int i = 0; i < llarg;i++)
    qua[i] = -1.0f;


 /* Comprobracion y apertura del stream a la tarjeta SD */
   if(compruebaTarjeta(pin))
    Serial.println("Tarjeta OK");
   else
   {
    Serial.println("Error al abrir la tarjeta.");
    return;
   }
    
    /* Obtener el String correspondiente al Json con los datos de 
     *  configuracion en la tarjeta */
    configuracio = (char*)malloc(largofichero);
    leerFichero("/dades.txt",configuracio,largofichero);
    String configuracions =(String)configuracio;
    Serial.println("Datos.txt");
    Serial.println(configuracio);    
    Serial.println("Config " + configuracions);
    
    /* Deserializar el json y meter los datos en una array de cadenas */
    const char ** lista = (const char**)malloc(sizeof(char*)*10);
    deserializaJson(configuracions,lista); 
    Serial.println("DATOS:");
    for(int i = 0; i < 6;i++)
      Serial.println(lista[i]);  
      
    /* Establecer el valor de los datos de conexion a la red wifi */
    setSsid(lista[0]);
    setPwd(lista[1]);

    String h1 = lista[2];
    String h2 = lista[3];
    String h3 = lista[4];
    String h4 = lista[5];
    String p = lista[6];
    int h1i = h1.toInt();
    int h2i = h2.toInt();
    int h3i = h3.toInt();
    int h4i = h4.toInt();
    host[0] = (byte)h1i;
    host[1] = (byte)h2i;
    host[2] = (byte)h3i;
    host[3] = (byte)h4i;
    port = p.toInt();
    
    Serial.println("Host: ");
    Serial.print(host[0]);
    Serial.print(",");
    Serial.print(host[1]);
    Serial.print(",");
    Serial.print(host[2]);
    Serial.print(",");
    Serial.print(host[3]);
    Serial.print('\n');
    Serial.println("PORT:");
    Serial.print(port);
    
    /* Connectar a la xarxa Wifi */
    conectaWifi();

 // Definicio de tasques periodiques
 
  xTaskCreate(
                    wifiCheck,     // Task function. 
                    "wifiCheck",        // String with name of task. 
                    10000,            // Stack size in bytes. 
                    NULL,             // Parameter passed as input of the task 
                    1,                // Priority of the task. 
                    NULL);            // Task handle. 
                    
  
  xTaskCreate(
                    pendreDades,      // Task function. 
                    "TaskTwo",        // String with name of task. 
                    10000,            // Stack size in bytes. 
                    NULL,             // Parameter passed as input of the task 
                    1,                // Priority of the task. 
                    NULL);            // Task handle. 
                   
 xTaskCreate(
                    dadesAInflux,        // Task function. 
                    "dadesAInflux",      // String with name of task. 
                    10000,            // Stack size in bytes. 
                    NULL,             // Parameter passed as input of the task 
                    1,                // Priority of the task. 
                    NULL);
}

void loop(){} // Sense utilitat en aquest programa


////////////////////////////////
//
//  MOSTRA EN EL PORT SERIE EL RESULTAT DEL Irms I LA Potencia
//
//////////////////////////////
//
void printMeasure(String prefix, float value, String postfix)
{
 Serial.print(prefix);
 Serial.print(value, 3);
 Serial.println(postfix);
}

////////////////////////////////
//
//  BUCLE QUE OBTÉ EL Irms ( INTENSITAT ) AL SCT , CALCULA LA POTENCIA
//  AMB EL VOLTATGE I A CADA SEGON LES MOSTRA I AFEFEIX LA POTENCIA A LA QUA
//
//////////////////////////////
//
void pendreDades (void * parameter)
{
 while(true)
 {
  float currentRMS = getCorriente();
  float power = 220.0 * currentRMS;
  printMeasure("Irms: ", currentRMS, "A ,");
  printMeasure("Potencia: ", power, "W");
  afegirAQua(power,qua,&nelements,llarg);
  delay(1000);
 }
 vTaskDelete( NULL );
}

////////////////////////////////
//
//  FA LECTURES DE LA CORRENT ( INTENSITAT ) AL SCT DURANT tempsestablert MILISEGONS I RETORNA 
//  LA MITJA QUADRATICA DE LES MEDICIONS
//
//////////////////////////////
//
float getCorriente()
{
 float voltage;
 float corriente;
 float sum = 0;
 long tiempo = millis();
 int counter = 0;
 float lecturamax = 0;
 int tempsestablert = 2000;
 while (millis() - tiempo < tempsestablert)
 {
   float lectura = 0.0f;
   voltage = readVoltage(&lectura);
   if(lectura > lecturamax)
       lecturamax = lectura;
   corriente = voltage * FACTOR;
   corriente /= 1000.0;
 
   sum += sq(corriente);
   counter = counter + 1;
  }
 Serial.println("Difencial màxim al ADS1115: ");
 Serial.println(lecturamax);
 corriente = sqrt(sum / counter);
 return(corriente);
}

////////////////////////////////
//
//  OBTÉ EL DIFERENCIAL AL ADS1115, EL FICA A LA VARIABLE
//  lectura EL PUNTER DEL QUAL ES PASSA COM A ARGUMENT, ESTABLEIX
//  UN FACTOR DE CORRECCIÓ EN FUNCIO DEL VALOR OBTINGUT I RETORNA 
//  EL VALOR MULTIPLICAT PEL FACTOR COM A RESULTAT.
//
//////////////////////////////
//
float readVoltage(float * lectura)
{
  float multiplier = 0;
  *lectura = abs(ads.readADC_Differential_0_1());
  if(*lectura < 10)
      multiplier = 0.0f;
   else if (*lectura > 10 && *lectura < 50)
      multiplier = 0.0535F;
   else if (*lectura > 50 && *lectura < 150)
      multiplier = 0.1093F;
   else if (*lectura > 150 && *lectura < 500)
      multiplier = 0.0855F;
   else if (*lectura > 500 && *lectura < 3000)
      multiplier = 0.0968F;
   else
      multiplier = 0.0955F;
   float voltage = (*lectura) * multiplier;
   return voltage;
}

///////////////////////////////////
//
//  COMPROBA QUE LA QUA NO ESTIGUI BUIDA, I 
//  SI ES AIXÍ, TREU EL PRIMER VALOR
//  PER ENVIAR-LO A INFLUXDB
//
/////////////////////////////
//
void dadesAInflux( void * parameter)
{
  float valor = -1.0;
  String linia;
  
  while(true)
  {
    if(! quaBuida(nelements))
    {
      valor =  treureDeQua(qua,&nelements,llarg);
      char cadenavalor[16];
      sprintf(cadenavalor,"%f",valor);      
      linia = "consumllum watts=" + String(cadenavalor); //Taula (consumllum), camp (Watts) i valor      
      // Enviament del packet a influxDB
      Serial.println("Enviant UDP packet...");
      Serial.println(linia);

      
      udp.beginPacket(host, port);
      udp.print(linia);
      udp.endPacket();
    }
     vTaskDelay(10);
  }
  
  vTaskDelete( NULL );
}
 
