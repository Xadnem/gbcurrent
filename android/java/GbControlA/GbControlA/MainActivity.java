package GbControlA.GbControlA;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toolbar;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;


public class MainActivity extends AppCompatActivity implements MqttCallback, IMqttActionListener
{
    private static final String TAG = "MainActivity";
    private TextView tbCodi,lbEstado,lbEstadoGbControl;
    private Intent dialogopassword,pantallaconsums,grafic;
    private GbMqtt mqttclient;
    private WebView wbCamara;
    private boolean enviardatos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Inicialització dels atributs de la classe */
        Button btCamaraOn = findViewById(R.id.btCamaraOn);
        btCamaraOn.setBackgroundColor(Color.rgb(153,51,255));
        btCamaraOn.setTextColor(Color.CYAN);
        btCamaraOn.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              mqttclient.publishMessage(getResources().getString(R.string.topicActivacionCamara),
                                                      getResources().getString(R.string.activarCamara));
                                              visualizarCamara();
                                          }
                                      });
        Button btCamaraOff = findViewById(R.id.btCamaraOff);
        btCamaraOff.setBackgroundColor(Color.rgb(160,160,160));
        btCamaraOff.setTextColor(Color.BLACK);
        btCamaraOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mqttclient.publishMessage(getResources().getString(R.string.topicActivacionCamara),
                        getResources().getString(R.string.desActivarCamara));
                ocultaCamara();
            }
        });
        Button btEnviament = findViewById(R.id.btEnviament);
        btEnviament.setBackgroundColor(Color.rgb(0,255,255));
        btEnviament.setTextColor(Color.BLUE);
        btEnviament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String missatge = tbCodi.getText().toString();
                mqttclient.publishMessage(GestorArchivos.getTOPIC(),missatge);
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(tbCodi.getWindowToken(), 0);
                btCamaraOff.setVisibility(View.VISIBLE);
                btCamaraOn.setVisibility(View.VISIBLE);
            }
        });

        tbCodi = findViewById(R.id.tbCodi);
        tbCodi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    btCamaraOff.setVisibility(View.INVISIBLE);
                    btCamaraOn.setVisibility(View.INVISIBLE);
                } else {
                    btCamaraOff.setVisibility(View.VISIBLE);
                    btCamaraOn.setVisibility(View.VISIBLE);
                }
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        /* Implementar el listener del menu del ToolBar */
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId() == R.id.menuconfiguracion)
                {
                    startActivity(dialogopassword);
                }
                else if(item.getItemId() == R.id.cerrarAplicacion)
                {
                    MainActivity.this.finishAffinity();
                    System.exit(0);
                }
                else if(item.getItemId() == R.id.reniniciESP32)
                {
                    mqttclient.publishMessage(getResources().getString(R.string.topicReiniciaPlaca),"");
                }
                else if(item.getItemId() == R.id.consums)
                {
                    startActivity(pantallaconsums);
                }
                return false;
            }
        });
        /* Inicialitzar el ToobBar */
        toolbar.inflateMenu(R.menu.toolbarmenu);

        /* Inicialitzar l'etiqueta d'estat de conexió amb el broker */
        lbEstado = findViewById(R.id.lbEstadoConexion);
        lbEstado.setBackgroundColor(Color.RED);
        lbEstado.setText(R.string.estadoDesconectado);
        lbEstado.setInputType(InputType.TYPE_NULL);

        /* Inicialitzar l'etiqueta d'estat del sistema de control d'access */
        lbEstadoGbControl = findViewById(R.id.lbEstadoGbControl);
        lbEstadoGbControl.setText(getResources().getString(R.string.estadoDesactivado));
        lbEstadoGbControl.setBackgroundColor(Color.YELLOW);
        lbEstadoGbControl.setInputType(InputType.TYPE_NULL);

        /* Inicialitzar el intent per a la activity de la contrasenya per a la configuració */
        dialogopassword = new Intent(getApplicationContext(), DialogoPWD.class);

        /* Inicialitzar el intent per a la activity de la pantalla de la pressa de dades del gràfic de consums */
        pantallaconsums = new Intent(getApplicationContext(),PantallaConsums.class);

        /* Inicialitzar el intent per a la activity de la pantalla del gràfic de consums */
        grafic = new Intent(getApplicationContext(), GraficConsums.class);

        /* Carregar la configuracó des del fitxer intern de l'aplicació */
        File ficheroconfiguracion = new File(this.getFilesDir(),getResources().getString(R.string.datosRuta));
        GestorArchivos.leerConfiguracion(ficheroconfiguracion,this);

        /* Inicialitzar l'objecte mqtt client de la classe */
        mqttclient = GbMqtt.obtenerClienteMqtt(this,this);

        /* Comprovar si l'inci ve d'un Intent en PantallaConfiguracion i escriure la nova
           configuració si cal */
        Bundle parametros = this.getIntent().getExtras();
        enviardatos = false;
        if(parametros != null) {
            enviardatos = parametros.getBoolean("DatosOK");//Si es ve de la pantalla de
                                                           // configuració posar enviardatos a true
        }
    }

    @Override
    public void onBackPressed() {
        MainActivity.this.finishAffinity();
        System.exit(0);
    }

    //////////////////////////
    //
    //  S'EXECUTA QUAN L'USUARI A ANAT A UN ALTRE ACTIVITY I TORNA A AQUESTA
    //
    ////////////////////////////////////
    //
    @Override
    protected void onResume() {
        super.onResume();
      mqttclient = GbMqtt.obtenerClienteMqtt(this,this);//Por si el usuario ha cambiado los datos relativos al broker del fichero de configuracion
        if(mqttclient.isConnected()) {
            mqttclient.publishMessage(getResources().getString(R.string.topicEstadoGbcontrol), getResources().getString(R.string.consultaEstat));
            mqttclient.publishMessage(getResources().getString(R.string.topicCheckCodi), getResources().getString(R.string.consultaCodi));
        }
    }

    //////////////////////////////////////////
    //
    //  VISUALITZA EL WEBVIEW DE L'INSTANCIA I REALITZA INTENTS DE CARREGA DE LA URL A LA QUE
    //  TRANSMET EL SERVIDOR HTTP DE GbControl
    //
    ////////////////////////////////////////////////
    //
    private void visualizarCamara()
    {
        wbCamara = findViewById(R.id.wvCamara);
        wbCamara.setVisibility(View.VISIBLE);
        String url = "http://" + GestorArchivos.getIP()+":8081";
        wbCamara.setWebViewClient(new WebViewClient()
        {
            int contador = 0;
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
                while(error.getErrorCode() == -6 && contador < 10000) {
                    wbCamara.loadUrl(url);
                    contador++;
                }
            }
        });
        wbCamara.loadUrl(url);
    }

    private void ocultaCamara()
    {
        wbCamara.clearCache(true);
        wbCamara.clearFormData();
        wbCamara.clearHistory();
        wbCamara.loadUrl("about:blank");
        wbCamara.setVisibility(View.INVISIBLE);
    }


    /////////////////////////////////
    //
    //  ENVIA UN JSON AMB LES DADES DE CONFIGURACIÓ I EL CODI DE DESACTIVACIÓ ACTUAL AL BROKER
    //
    //////////////////////////////////////
    //
    private void enviarConfiguracion()
    {
        Context context = this;
        // Crear json para con los datos de configuración para enviar por mqtt al programa en la Raspberry
        try {
            GestorArchivos.crearJsonConfiguracion(context);
            mqttclient.publishMessage(getResources().getString(R.string.topicEnvioConfiguracion),GestorArchivos.getJsonConfiguracion());
            mqttclient.publishMessage(getResources().getString(R.string.topicCodiCanviat),GestorArchivos.getCODIGODESACTIVACION());
            Log.d(TAG, "DATOS DE CONFIGURACION ENVIADOS");
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG, "NO SE HAN PODIDO ENVIAR LOS DATOS DE CONFIGURACION");
        }
    }

    /* Implementació deLs métodes de l'interficie MqttCallBack del objecte GbMqtt */

    @Override
    public void connectionLost(Throwable cause) {
       mostrarEstado(false);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        Log.d(TAG, "deliveryComplete: ");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Log.d(TAG, "Incoming message: " + new String(message.getPayload()) + " Topic " + topic);
        if (topic.equals(GestorArchivos.getTOPICRECEPCION())) {
            String mensaje = new String(message.getPayload());
            tbCodi.setText(mensaje);
        } else if (topic.equals(getResources().getString(R.string.topicEstadoGbcontrol))) {
            String respuesta = new String(message.getPayload());
            if (respuesta.equals("Warning")) {
                lbEstadoGbControl.setBackgroundColor(Color.RED);
                visualizarCamara();
            } else if (respuesta.equals("Armat")) {
                lbEstadoGbControl.setBackgroundColor(Color.GREEN);
                tbCodi.setText("");
                lbEstadoGbControl.requestFocus();
            }
            else if (respuesta.equals("Desarmat")) {
                lbEstadoGbControl.setBackgroundColor(Color.YELLOW);
                lbEstadoGbControl.requestFocus();
                lbEstadoGbControl.setText(respuesta);
                ocultaCamara();
            }
            if(!respuesta.equals(getResources().getString(R.string.consultaEstat)))
                lbEstadoGbControl.setText(respuesta);
        }
        else if(topic.equals(getResources().getString(R.string.topicCodiCanviat))) 
        {
            File ficheroconfiguracion = new File(this.getFilesDir(),getResources().getString(R.string.datosRuta));
            GestorArchivos.canviarCodiDesactivacio(ficheroconfiguracion,this,message.toString());
            Log.d(TAG, "CANVI CODI" + topic + "  " + message.toString());
        }
        else if(topic.equals(getResources().getString(R.string.topicCheckCodi)) &&
                !(new String(message.getPayload())).equals(getResources().getString(R.string.consultaCodi)))
            comprobaCodi(new String(message.getPayload()));
        else if (topic.equals("DadesInflux")) {
            ClaseUtilidad.dadesInflux = new String(message.getPayload());
            ClaseUtilidad.parseDadesInflux();
            if(!GraficConsums.iniciat) { // Per evitar iniciar l'activity grafic més d'una vegada
                startActivity(grafic);
                GraficConsums.iniciat = true;
            }
        }
        else if(topic.equals("InfluxMax"))
        {
            ClaseUtilidad.influxMax = Float.parseFloat(new String(message.getPayload()));
        }
        else if(topic.equals("InfluxUnits"))
        {
            ClaseUtilidad.influxUnits = new String(message.getPayload());
        }
        else if(topic.equals("InfluxConsum"))
        {
            ClaseUtilidad.InfluxConsum = new String(message.getPayload());
        }
        else if(topic.equals("InfluxStamps"))
        {
            ClaseUtilidad.parseDatasInflux(new String(message.getPayload()));
        }

    }
    
	////////////////////////
    //
    //  COMPRUEBA SI EL CODIGO DE DESACTIVACION EN EL BROKER ES EL MISMO QUE HAY EN EL DISPOSITIVO Y SI NO ES
    //  ASI, LLAMA A LA FUNCION EN GESTORARCHIVOS PARA CAMBIARLO
    //
    ////////////////////////////////////////////////
    //
    private void comprobaCodi(String codienbroker) throws IOException, IOException {
        if(!GestorArchivos.getCODIGODESACTIVACION().equals(codienbroker))
            GestorArchivos.canviarCodiDesactivacio(new File(this.getFilesDir(),getResources().getString(R.string.datosRuta)),this,codienbroker);
    }




    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        Log.d(TAG, "onSuccess" + " Connected to " + mqttclient.getServerURI());
        try {
            Log.d(TAG, "Suscribiendo a: " + GestorArchivos.getTOPICRECEPCION());
            mqttclient.subscribe(GestorArchivos.getTOPIC(), GbMqtt.QOS);
            mqttclient.subscribe(GestorArchivos.getTOPICRECEPCION(),GbMqtt.QOS);
            mqttclient.subscribe(getResources().getString(R.string.topicEstadoGbcontrol),GbMqtt.QOS);
            mqttclient.subscribe(getResources().getString(R.string.topicCodiCanviat),GbMqtt.QOS);
            mqttclient.subscribe(getResources().getString(R.string.topicCheckCodi),GbMqtt.QOS);
            mqttclient.subscribe("DadesInflux",GbMqtt.QOS);
            mqttclient.subscribe("InfluxMax",GbMqtt.QOS);
            mqttclient.subscribe("InfluxUnits",GbMqtt.QOS);
            mqttclient.subscribe("InfluxConsum",GbMqtt.QOS);
            mqttclient.subscribe("InfluxStampInici",GbMqtt.QOS);
            mqttclient.subscribe("InfluxStamps",GbMqtt.QOS);
            mostrarEstado(true);
            mqttclient.setCallback(this);
            /* Comprovar l'estat de GbControl */
            mqttclient.publishMessage(getResources().getString(R.string.topicEstadoGbcontrol),
                    getResources().getString(R.string.consultaEstat));
            mqttclient.publishMessage(getResources().getString(R.string.topicCheckCodi),
                    getResources().getString(R.string.consultaCodi));
            /* Enviar les dades de configuració si es ve del Intent per a PantallaConfiguracion */
            if(enviardatos)
                enviarConfiguracion();
        } catch (Exception e) {
            Log.e(TAG, "Error subscribing to topic", e);
            mostrarEstado(false);
        }
    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        Log.e(TAG, "Failed to connect to: " + GestorArchivos.getIP(), exception);
        mostrarEstado(false);
    }

    ////////////////////////////////
    //
    //  MOSTRA L'ESTAT DE CONEXIÓ A LA ETIQUETA lbEstado
    //
    ////////////////////////////////////////////
    //
    private void mostrarEstado(boolean conectado)
    {
        if(conectado) {
            lbEstado.setBackgroundColor(Color.GREEN);
            lbEstado.setText(R.string.estadoConectado);
        }
        else
        {
            lbEstado.setBackgroundColor(Color.RED);
            lbEstado.setText(R.string.estadoDesconectado);
        }
    }

    public GbMqtt getMqttclient()
    {
        return this.mqttclient;
    }

}
