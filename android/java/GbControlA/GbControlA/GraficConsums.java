package GbControlA.GbControlA;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GraficConsums extends AppCompatActivity
{
    Grafic grafic;
    Button btFinalitzar;
    private int ample,alt;
    static boolean iniciat = false; // Per establir quan hi ha un grafic actiu i evitar que s'obri més d'una vegada.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        grafic = new Grafic(this);
        setContentView(R.layout.layoutgrafic);
        View layout = findViewById(R.id.layoutgrafic);
        ((ConstraintLayout)layout).addView(grafic);

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        alt = displayMetrics.heightPixels;
        ample = displayMetrics.widthPixels;

        btFinalitzar = findViewById(R.id.btFinalizar);
        btFinalitzar.setBackgroundColor(Color.rgb(0,0,255));
        btFinalitzar.setTextColor(Color.rgb(0,230,255));
        btFinalitzar.setHeight(20);
        btFinalitzar.setWidth(ample/2);
    }

    public void btFinalizar_onClick(View v)
    {
        GraficConsums.iniciat = false;
        this.finish();
    }

    ////////////////////////////////////
    ///
    /// CLASSE INTERNA PER EL CANVAS AMB EL GRAFIC
    ///
    /////////////////////////////////////////////
    //
    public class Grafic extends View
    {
        Paint pincelleixos, pincellRetols,pincelldivisions;
        public Grafic(Context context)
        {
            super(context);
            pincelleixos = new Paint();
            pincellRetols = new Paint();
            pincelldivisions = new Paint();
        }

        @Override
        protected void onDraw(Canvas canvas) {

            // Establir els factors d'escala
            float escalah = (alt-300) / ((float)ClaseUtilidad.llistaDades.size()-1);
            float escalav = (ample-200) / ClaseUtilidad.influxMax;

            //Inicialitzar els pinzells
            pincelleixos.setColor(Color.WHITE);
            pincelleixos.setStrokeWidth(10);
            pincelleixos.setStyle(Paint.Style.FILL);

            pincellRetols.setColor(Color.CYAN);
            pincellRetols.setStrokeWidth(2);
            pincellRetols.setStyle(Paint.Style.FILL);
            pincellRetols.setTextSize(29f);

            pincelldivisions.setColor(Color.WHITE);
            pincelldivisions.setStrokeWidth(2);
            pincelldivisions.setStyle(Paint.Style.FILL);
            pincelldivisions.setTextSize(19f);

            canvas.drawColor(Color.BLACK);

            // Dibuixar les unitats de consum
            String retoldades = (ClaseUtilidad.InfluxTipusDada.equals("Llum"))?"Watts":"Llitres";
            canvas.drawText(retoldades,(ample/2f)-40,alt-170,pincellRetols);

            // Establir l'inici i final dels eixos
            int origenx = ample-150;
            int origeny = alt-260;
            int fiordenades = 50;
            int fiabcises = 40;

            // Dibuixar els eixos
            canvas.drawLine(origenx,origeny,fiordenades,origeny,pincelleixos); //Eix Y
            canvas.drawLine(origenx,origeny,origenx,fiabcises,pincelleixos); //Eix X

            // Dibuixar les linies de divisio de l'escala del eix d'abscisses
            for(int i = 0;i < ClaseUtilidad.llistaDades.size();i++)
            {
                float inicilinia = (float)origenx - 25f;
                float filinia = (float)origenx + 25f;
                float situacioy = (float)origeny - (((float)i)*escalah);
                canvas.drawLine(inicilinia,situacioy,filinia,situacioy,pincelldivisions);
                float hora = ClaseUtilidad.llistaDades.get(i).get(0);
                int horaint = (int)hora;
                canvas.drawText(Integer.toString(horaint),filinia+10,situacioy+5,pincelldivisions);
            }

            // Girar el canvas
            canvas.rotate(-90,0,0);
            canvas.translate(-alt+10,0);

            // Reasignar els origens i final dels eixos
            origenx = 250;
            origeny = ample-150;
            fiabcises = alt-50;
            fiordenades = 50;

            // Dibuixar les unitats temporals
            canvas.drawText(ClaseUtilidad.influxUnits,(fiabcises/2f)+75,ample-30,pincellRetols);

            //Dibuixar el maxim;
            pincellRetols.setColor(Color.YELLOW);
            float situaciox = ClaseUtilidad.indexmax*escalah;
            float situacioy = ClaseUtilidad.influxMax*escalav;
            canvas.drawText("MAX: " + Math.round(ClaseUtilidad.influxMax),origenx + situaciox,
                    origeny - situacioy-20,pincellRetols );
            canvas.drawOval(origenx+situaciox-10,origeny-situacioy-10,origenx+situaciox+10,
                    origeny-situacioy+10,pincellRetols);

            // Establir l'espaiat entre les linies de l'escala de valors
            float espaiatv = 0;
            float max = ClaseUtilidad.influxMax;
            if(max <= 50)
                espaiatv = escalav;
            else if(max > 50 && max <= 100)
                espaiatv = 10 * escalav;
            else if(max > 100 && max <= 300)
                espaiatv = 20 * escalav;
            else if(max > 300 && max <= 400)
                espaiatv = 25 * escalav;
            else if(max > 400 && max <= 500)
                espaiatv = 30 * escalav;
            else if(max > 500 && max <= 750)
                espaiatv = 50 * escalav;
            else if(max > 750 && max <= 1000)
                espaiatv = 70 * escalav;
            else if(max > 1000 && max <= 1500)
                espaiatv = 100 * escalav;
            else
                espaiatv = 150 * escalav;

            // Dibuixar les linies de l'escala del eix d'ordenades
            int contador = 0;
            pincellRetols.setColor(Color.LTGRAY);
            pincellRetols.setStrokeWidth(1f);
            while(contador*espaiatv < origeny - fiordenades)
            {
                float valor = (espaiatv / escalav) * contador;
                canvas.drawLine(origenx,origeny - contador*espaiatv,fiabcises,
                        origeny - contador*espaiatv,pincellRetols);
                canvas.drawText(Integer.toString((int)Math.floor(valor)),origenx-50,
                        origeny - contador*espaiatv,pincelldivisions);
                contador++;
            }

            // Dibuixar les linies del gràfic
            float puntinix = origenx;
            float situacioh = origenx;
            float puntiniy = origeny - ClaseUtilidad.llistaDades.get(0).get(1)*escalav;
            pincellRetols.setColor(Color.RED);
            pincellRetols.setStrokeWidth(5);
            for(ArrayList<Float> l:ClaseUtilidad.llistaDades)
            {
                float puntfix = situacioh;
                float puntfiy = origeny - l.get(1)*escalav;
                canvas.drawLine(puntinix,puntiniy,puntfix,puntfiy,pincellRetols);
                puntinix = puntfix;
                puntiniy = puntfiy;
                situacioh += escalah;
            }

            // Dibuixar la mitja
            pincellRetols.setColor(Color.GREEN);
            pincellRetols.setStrokeWidth(3);
            situacioy = origeny - ClaseUtilidad.influxMitja * escalav;
            canvas.drawLine(origenx,situacioy,fiabcises,situacioy,pincellRetols);
            canvas.drawText("Mitja: " + Math.round(ClaseUtilidad.influxMitja),fiabcises-150,
                    situacioy-20,pincellRetols);

            // Dibuixar la data i hora inicials
            pincelldivisions.setColor(Color.CYAN);
            canvas.drawText(ClaseUtilidad.datainici,origenx-50,origeny+110,pincelldivisions);
            canvas.drawText(ClaseUtilidad.horainici,origenx-50,origeny+130,pincelldivisions);

            // Dibuixar la data i hora finals
            canvas.drawText(ClaseUtilidad.datafinal,fiabcises-100,origeny+110,pincelldivisions);
            canvas.drawText(ClaseUtilidad.horafinal,fiabcises-100,origeny+130,pincelldivisions);

            // Dibuixar el consum del periode
            canvas.drawText("Kwh: " + ClaseUtilidad.InfluxConsum,origenx + 200,origeny+110,pincellRetols);
        }
    }
}
