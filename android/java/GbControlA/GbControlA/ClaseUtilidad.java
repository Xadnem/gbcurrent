package GbControlA.GbControlA;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

public class ClaseUtilidad
{
    static AlertDialog dialog;
    final static String TAG = "ClaseUtilidad";
    static float influxMitja,influxMax;
    static String dadesInflux,influxUnits,InfluxTipusDada,InfluxConsum,datainici,datafinal,horainici
            ,horafinal;
    static ArrayList<ArrayList<Float>> llistaDades;
    static int indexmax;


    public static void mostrarMessageBox(Context contexto, String titulo, String mensaje)
    {
        dialog = new AlertDialog.Builder(contexto) // Pass a reference to your main activity here
                .setTitle(titulo)
                .setMessage(mensaje)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    //////////////////////////////////////////////
    //
    // OMPLE LA LLISTA DE LLISTES DADA TEMPORAL / VALOR llistaDades DE LA CLASSE SEGONS EL STRING
    // CORRESPONENT AL JSON AMB LES DADES ENVIAT PER GbControl.
    //
    //////////////////////////////////////////////////////////////////
    //
    public static void parseDadesInflux()
    {
        influxMitja = 0;
        llistaDades = new ArrayList<ArrayList<Float>>();
        int indiceini = 2;
        int indicefin = dadesInflux.indexOf(']');
        while(indicefin < dadesInflux.length()-1) {
            String dada = dadesInflux.substring(indiceini, indicefin - 1);
            //Log.d(TAG, "DADA: " + dada);
            int indicecoma = dada.indexOf(',');
            ArrayList<Float> tupla = new ArrayList<>();
            tupla.add(Float.parseFloat(dada.substring(0,indicecoma)));
            float valor = Float.parseFloat(dada.substring(indicecoma+1));
            influxMitja += valor;
            if(valor == influxMax)
                indexmax = llistaDades.size();
            tupla.add(valor);
            //Log.d(TAG, Float.toString(tupla.get(0)));
            //Log.d(TAG, Float.toString(tupla.get(1)));
            llistaDades.add(tupla);
            indiceini = dadesInflux.indexOf('[',indiceini+1)+1;
            indicefin = dadesInflux.indexOf(']',indicefin+1);
            //Log.d(TAG, "Length: " + Integer.toString(dadesInflux.length()) + " IndiceFin: " + Integer.toString(indicefin));
        }
        if(llistaDades.size() > 0)
            influxMitja /= llistaDades.size();
    }

    /////////////////////////////////
    //
    //  ASIGNA ELS STRINGS DE LA CLASSE datainici,datafinal,horainici i horafinal
    //  SEGONS ELS DOS TIMESTAMP SEPARATS PER UN ESPAI, PASSATS COM A ARGUMENT
    //
    ///////////////////////////////
    //
    public static void parseDatasInflux(String timestamps)
    {
        int indexsep = timestamps.indexOf(' ');
        String dataini = timestamps.substring(0,indexsep);
        String datafi = timestamps.substring(indexsep+1,timestamps.length());
        /*
        //Obtenir i assignar la data d'inici
        String anyini = ClaseUtilidad.parseYear(dataini);
        String mesini = ClaseUtilidad.parseMonth(dataini);
        String diaini = ClaseUtilidad.parseDay(dataini);
        datainici = diaini + "-" + mesini + "-"+anyini;
         */
        datainici = ClaseUtilidad.construeixData(dataini);
        horainici = ClaseUtilidad.construeixHoraCat(dataini);
        datafinal = ClaseUtilidad.construeixData(datafi);
        horafinal = ClaseUtilidad.construeixHoraCat(datafi);
        /*
        //Obtenir i assignar la data final
        String anyfi = ClaseUtilidad.parseYear(datafi);
        String mesfi = ClaseUtilidad.parseMonth(datafi);
        String diafi = ClaseUtilidad.parseDay(datafi);
        datafinal = diafi + "-" + mesfi + "-"+anyfi;
         */
        // Obtenir i assignar l'hora,minut i segon inicials
        /*
        String horaini = ClaseUtilidad.parseHour(dataini);
        int horainiutc = Integer.parseInt(horaini)+2;
        if(horainiutc > 24)
            horainiutc = horainiutc % 24;
        horaini = Integer.toString(horainiutc);
        String minutini = ClaseUtilidad.parseMinut(dataini);
        String segonini = ClaseUtilidad.parseSeg(dataini);
        horainici = horaini + ":"+minutini + ":"+segonini;

        // Obtenir i assignar l'hora,minut i segon finals
        String horafi = ClaseUtilidad.parseHour(datafi);
        int horafiutc = Integer.parseInt(horafi)+2;
        if (horafiutc > 24)
            horafiutc = horainiutc % 24;
        horafi = Integer.toString(horafiutc);
        String minutfi = ClaseUtilidad.parseMinut(datafi);
        String segonfi = ClaseUtilidad.parseSeg(datafi);
        horafinal = horafi + ":"+minutfi + ":"+segonfi;
         */
    }
    ///////////////////////////////////////////
    //
    // FORMATA LA DATA EXTRETA D'UN TIMESTAMP DE INFLUXDB PASADA COM ARGUMENT
    // EN FORMAT DD-MM-AAAA Y LA RETORNA COM UN NOU STRING
    //
    //////////////////////////////////////////////////
    private static String construeixData(String timeStamp)
    {
        String any = ClaseUtilidad.parseYear(timeStamp);
        String mes = ClaseUtilidad.parseMonth(timeStamp);
        String dia = ClaseUtilidad.parseDay(timeStamp);
        return dia + "-" + mes + "-"+any;
    }

    ///////////////////////////////////////
    //
    //  PASA L'HORA UTC DEL TIMESTAMP DE INFLUXDB PASAT COM A ARGUMENT A UTC+2 Y LA
    //  FORMATA EN UN NOU STRING DE MANERA ADIENT PER A SER MOSTRADA AL GRAFIC
    //
    //////////////////////////////////////////////////
    //
    private static String construeixHoraCat(String timeStamp)
    {
        String hora = ClaseUtilidad.parseHour(timeStamp);
        int horacat = Integer.parseInt(hora)+2;
        if(horacat > 24)
            horacat = horacat % 24;
        hora = Integer.toString(horacat);
        String minut = ClaseUtilidad.parseMinut(timeStamp);
        String segon = ClaseUtilidad.parseSeg(timeStamp);
       return hora + ":"+minut + ":"+segon;
    }

    private static String parseYear(String timestamp)
    {
        return timestamp.substring(0,4);
    }
    private static String parseMonth(String timestamp)
    {
        return timestamp.substring(5,7);
    }
    private static String parseDay(String timestamp)
    {
        return timestamp.substring(8,10);
    }
    private static String parseHour(String timestamp)
    {
        return timestamp.substring(11,13);
    }
    private static String parseMinut(String timestamp)
    {
        return timestamp.substring(14,16);
    }
    private static String parseSeg(String timestamp)
    {
        return timestamp.substring(17,19);
    }
}
