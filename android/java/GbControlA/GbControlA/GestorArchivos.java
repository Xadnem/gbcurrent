package GbControlA.GbControlA;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import org.json.*;


public class GestorArchivos
{
  private static String SERVER_IP ="127.0.0.1";
  private static String PORT = "1883";
  private static String TOPIC = "";
  private static String TOPICRECEPCION = "";
  private static String PWD = "123";
  private static String USER = "yo";
  private static String CODIGODESACTIVACION = "";
  private static String jsonConfiguracion;

  ////////////////////////////////
  //
  //  ESCRIU LES DADES CLAU:VALOR CONTINGUTS EN EL HASHMAP QUE ES PASSA COM A PRIMER ARGUMENT, EN
  //  EL FITXER DE CONFIGURACIÓ INTERN DE L'APLICACIÓ LA RUTA DEL QUAL ES PASSA COM A SEGON
  //
  /////////////////////////////////////////////
  //
  public static void escribirConfiguracion(HashMap<String,String> datos,String ruta,Context context) throws IOException {
    OutputStreamWriter fout = null;
    try
    {
      fout = new OutputStreamWriter(
              context.openFileOutput(ruta, Context.MODE_PRIVATE));
      for (Map.Entry<String, String> entry : datos.entrySet()) {
        fout.write(entry.getKey() + ":" + entry.getValue()+"\n");
      }
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }
    finally
    {
      if(fout != null)
          fout.close();
    }
  }

  //////////////////////////////////////////////
  //
  // LLEGEIX LES LINIES DE TEXT DEL FITXER DE CONFIGURACIO LA RUTA DEL QUAL ES PASSA COM A ARGUMENT,
  // SEPARA LA CLAU I EL VALOR DE CADA LINIA I ASSIGNA EL VALOR AL ATRIBUT DE LA CLASSE CORRESPONENT
  // EN FUNCIO DE LA CLAU
  //
  //////////////////////////////////////////
  //
  public static void leerConfiguracion(File fichero,Context context) {
    FileReader fr = null;
    BufferedReader br = null;
    try {
      // Obrir el fitxer i creacio del BufferedReader per a disposar del seu metode readline i fer
      // una lectura comoda.
      fr = new FileReader(fichero);
      br = new BufferedReader(fr);
      // Lectura del fitxer
      String linea;
      while ((linea = br.readLine()) != null) {
        int indice = linea.indexOf(':');
        String clave = linea.substring(0, indice);
        String valor = linea.substring(indice+1);
       if(clave.equals(context.getResources().getString(R.string.datosIp)))
            SERVER_IP = valor;
       else if(clave.equals(context.getResources().getString(R.string.datosPort)))
         PORT = valor;
       else if((clave.equals(context.getResources().getString(R.string.datosTopicCodigo))))
         TOPIC = valor;
       else if((clave.equals(context.getResources().getString(R.string.datosTopicRecepcion))))
         TOPICRECEPCION = valor;
       else if((clave.equals(context.getResources().getString(R.string.datosUser))))
         USER = valor;
       else if((clave.equals(context.getResources().getString(R.string.datosCodigo))))
         PWD = valor;
       else if((clave.equals(context.getResources().getString(R.string.datosCodigoDesactivacion))))
         CODIGODESACTIVACION= valor;
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (null != fr) {
          fr.close();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
    System.out.println("Fichero leido: " + SERVER_IP + "  " + PORT + "  " + TOPIC + "  " + USER +
            "  " + PWD);
  }
  
  /////////////////////////////
  //
  //  TORNA LA POSICIO EN BYTES A LA QUE ES TROBA EL CODI DE DESACTIVACIO EN EL FITXER DE CONFIGURACIO
  //
  //////////////////////////////////////
  //
  private static int localizarCodiDesactivacion(File fichero,Context context)
  {
    FileReader fr = null;
    BufferedReader br = null;
    String linea,clave = "";
    int puntero = 0;
    try
    {
      // Obrir el fitxer i creacio del BufferedReader per a disposar del seu metode readline i fer
      // una lectura comoda.
      fr = new FileReader(fichero);
      br = new BufferedReader(fr);
      // Lectura del fitxer
      while ((linea = br.readLine()) != null) {
        int indice = linea.indexOf(':'); // Obtindre l'index del caracter de separacio de la clau i el valor
        clave = linea.substring(0, indice);
        if (clave.equals(context.getResources().getString(R.string.datosCodigoDesactivacion))) { // quan es trobi la clau del codi, sortir del bucle
          break;
        }
        linea += '\n';
        puntero += linea.getBytes(StandardCharsets.UTF_8).length; // Anar afegint els bytes de cada linia
      }
      br.close();
      fr.close();
    }
    catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (null != fr) {
          fr.close();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
    // Afegir els bytes de la part de la clau de la linia on es troba el codi
    puntero += ":".getBytes().length;
    puntero += clave.getBytes(StandardCharsets.UTF_8).length;
    return puntero;
  }

  ////////////////////////////////
  //
  //  CANVIA EL CODI DE DESACTIVACIÓ EN EL FITXER INTERN DE L'APLICACIÓ
  //  PEL PASSAT COM A ARGUMENT
  //
  /////////////////////////////
  //
  public static void canviarCodiDesactivacio(File fichero,Context context,String noucodi) throws IOException {
    int posicion = localizarCodiDesactivacion(fichero,context);
    RandomAccessFile ficheroconfiguracion = new RandomAccessFile(fichero,"rw");
    ficheroconfiguracion.seek(posicion);
    ficheroconfiguracion.write(noucodi.getBytes());
    ficheroconfiguracion.seek(posicion);
    String prova = "";
    for(int i = 0; i < noucodi.length();i++)
    {
      prova += (char)ficheroconfiguracion.readByte();
    }
    System.out.println("Codigo cambiado a: " + prova);

    CODIGODESACTIVACION = noucodi;
    ficheroconfiguracion.close();
  }

  //////////////////////////////////////
  //
  //  METE LOS DATOS DE CONFIGURACION EN EL OBJETO JSON DE LA CLASE
  //
  /////////////////////////////////////////////////
  //
  public static void crearJsonConfiguracion(Context context) throws JSONException {
    JSONObject json = new JSONObject();
    json.put(context.getResources().getString(R.string.datosIp),SERVER_IP);
    json.put(context.getResources().getString(R.string.datosPort),PORT);
    json.put(context.getResources().getString(R.string.datosTopicCodigo),TOPIC);
    json.put(context.getResources().getString(R.string.datosCodigo),PWD);
    json.put(context.getResources().getString(R.string.datosUser),USER);
    json.put(context.getResources().getString(R.string.datosCodigoDesactivacion),CODIGODESACTIVACION);
    GestorArchivos.jsonConfiguracion = json.toString();
  }


  /* Getters */
  public static String getIP()
  {
    return SERVER_IP;
  }
  public static String getPORT()
  {
    return PORT;
  }
  public static String getTOPIC()
  {
    return TOPIC;
  }
  public static String getTOPICRECEPCION()
  {
    return TOPICRECEPCION;
  }
  public static String getPWD()
  {
    return PWD;
  }
  public static String getUSER()
  {
    return USER;
  }
  public static String getCODIGODESACTIVACION(){return CODIGODESACTIVACION;}
  public static String getJsonConfiguracion(){return GestorArchivos.jsonConfiguracion;}

}//Fi de la classe


