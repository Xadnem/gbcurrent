package GbControlA.GbControlA;
import android.content.Context;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttToken;
import org.eclipse.paho.client.mqttv3.MqttTopic;

import java.util.UUID;

public class GbMqtt extends MqttAndroidClient
{
    private static final String TAG = "GbMqtt";
    public static final int QOS = 1;
    private final MqttConnectOptions mqttConnectOptions;

    /////////////////////////////
    //
    //  CONSTRUCTOR
    //
    //////////////////////////////
    //
    public GbMqtt(Context context, String serverURI, String clientId) {
        super(context, serverURI, clientId);
        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setPassword(GestorArchivos.getPWD().toCharArray());
        mqttConnectOptions.setUserName(GestorArchivos.getUSER());
    }

    ///////////////////////////////
    //
    // TORNA UN OBJECTE GbMqtt CONSTRUIT AMB LA IP I EL PORT CONTINGUTS A LA CLASSE GestorArchivos
    // I AMBS ELS CALLBACKS PELS ESDEVENIMENTS onSucess I onFailure CONTINGUTS EN EL LISTENER
    // PASAT EN EL SEGON ARGUMENT
    //
    ////////////////////////////////////
    //
    public static GbMqtt obtenerClienteMqtt(Context context,IMqttActionListener listener)
    {
        String clientId = UUID.randomUUID().toString();
        Log.d(TAG, "onCreate: clientId: " + clientId);
        GbMqtt mqttclient = new GbMqtt(context, "tcp://" + GestorArchivos.getIP()+":"+GestorArchivos.getPORT(), clientId);
        mqttclient.Connect(listener);
        return mqttclient;
    }

    //////////////////////////////////
    //
    //  CONNECTA L'OBJECTE AL BROKER I PORT CONTINGUTS A LA CLASSE GestorArchivos AMB
    //  EL IMqttActionListener PARA LOS CALLBACK onSucess Y onFailure PASADO COMO ARGUMENTO
    //
    /////////////////////////////////////////
    //
    private void Connect(IMqttActionListener listener) {
        try {
            Log.d(TAG, "Connecting to " + GestorArchivos.getIP() + " Puerto: " + GestorArchivos.getPORT());
            this.connect(mqttConnectOptions, this, listener);
        } catch (MqttException ex) {
            Log.e(TAG, "Connect: ", ex);
        }
    }

    //////////////////////////////////
    //
    //  PUBLICA EL MISSATGE PASAT COM SEGON ARGUMENT AMB EL TOPIC PASAT COMO A PRIMER
    //
    ////////////////////////////////////////
    //
    public void publishMessage(String topic,String payload) {
        try {
            if (!this.isConnected()) {
                this.connect();
            }
            MqttMessage message = new MqttMessage();
            message.setPayload(payload.getBytes());
            message.setQos(QOS);
            this.publish(topic, message,null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.i(TAG, "publish succeed!") ;
                }
                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.i(TAG, "publish failed!") ;
                }
            });
        } catch (MqttException e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        }
    }





}
