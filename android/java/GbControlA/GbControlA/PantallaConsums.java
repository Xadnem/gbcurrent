package GbControlA.GbControlA;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class PantallaConsums extends AppCompatActivity implements IMqttActionListener
{
    RadioButton rbLlum,rbAigua;
    CalendarView calendariInici,calendariFinal;
    Spinner spHoraInici,spMinutInici,spSegonInici,spHoraFin,spMinutFin,spSegonFin;
    Button btAcceptar;
    String datainici,datafinal;
    GbMqtt gbMqtt;

    private static final String TAG = "PantallaConsums";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        GraficConsums.iniciat = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layoutconsums);
        rbLlum = findViewById(R.id.rbLlum);
        rbAigua = findViewById(R.id.rbAigua);
        calendariInici = findViewById(R.id.calendariInici);
        calendariInici.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                String diainici = String.valueOf(dayOfMonth);
                if(diainici.length()==1)
                    diainici = '0'+diainici;
                String mesinici = String.valueOf(month+1);
                if(mesinici.length()==1)
                    mesinici = '0'+mesinici;
                String anyinici = String.valueOf(year);
                datainici = anyinici + "-" + mesinici + "-"+diainici;
            }
        });
        calendariFinal = findViewById(R.id.calendariFinal);
        calendariFinal.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                String diafi = String.valueOf(dayOfMonth);
                if(diafi.length()==1)
                    diafi = '0'+diafi;
                String mesfi = String.valueOf(month+1);
                if(mesfi.length()==1)
                    mesfi='0'+mesfi;
                String anyfi = String.valueOf(year);
                datafinal = anyfi + "-" + mesfi + "-"+diafi;
            }
        });

        ArrayAdapter <CharSequence> adapterhores = ArrayAdapter.createFromResource(
                this,R.array.arrayHores, android.R.layout.simple_spinner_item);
        spHoraInici = findViewById(R.id.spHoraInici);
        spHoraInici.setAdapter(adapterhores);
        spHoraFin = findViewById(R.id.spHoraFin);
        spHoraFin.setAdapter(adapterhores);

        ArrayAdapter <CharSequence> adapterMinutsSegons = ArrayAdapter.createFromResource(
                this,R.array.arrayMinutsSegons, android.R.layout.simple_spinner_item);
        spMinutInici = findViewById(R.id.spMinutInici);
        spMinutInici.setAdapter(adapterMinutsSegons);
        spSegonInici = findViewById(R.id.spSegonInici);
        spSegonInici.setAdapter(adapterMinutsSegons);
        spMinutFin = findViewById(R.id.spMinutFin);
        spMinutFin.setAdapter(adapterMinutsSegons);
        spSegonFin = findViewById(R.id.spSegonFin);
        spSegonFin.setAdapter(adapterMinutsSegons);

        btAcceptar = findViewById(R.id.btAcceptar);
        btAcceptar.setBackgroundColor(Color.GREEN);

        gbMqtt = GbMqtt.obtenerClienteMqtt(this, this);
    }

    public void onRadioButtonClick(View view)
    {
        boolean checked = ((RadioButton)view).isChecked();
        switch(view.getId())
        {
            case R.id.rbLlum:
                if(checked)
                    Log.d(TAG, "Radio button Llum premmut");
                break;
            case R.id.rbAigua:
                if(checked)
                    Log.d(TAG, "Radio button Aigua premmut");
                break;
        }
    }

    public void btAceptarOnClick(View view) throws MqttException {
        //Construcció dels timestamp d'inici del periode a graficar
        if(datainici == null || datainici.length() == 0)
            datainici = LocalDateTime.now().toString()+'Z';
        else
        {
            if(datainici.contains("T"))
                datainici = datainici.substring(0,9);
            String horaini = (String)spHoraInici.getSelectedItem();
            String minutini = (String)spMinutInici.getSelectedItem();
            String segonini = (String)spSegonInici.getSelectedItem();
            datainici += 'T'+horaini+':'+minutini+':'+segonini+'Z';
        }

        //Construcció dels timestamp d'inici del periode a graficar
        if(datafinal == null || datafinal.length() == 0)
            datafinal = LocalDateTime.now().toString()+'Z';
        else
        {
            if(datafinal.contains("T"))
                datafinal = datafinal.substring(0,9);
            String horafi = (String)spHoraFin.getSelectedItem();
            String minutfi = (String)spMinutFin.getSelectedItem();
            String segonfi = (String)spSegonFin.getSelectedItem();
            datafinal += 'T'+horafi+':'+minutfi+':'+segonfi+'Z';
        }

        // Assignar les datas i hores d'inici i final a els atributs de la classe ClaseUtilidad
        String tipusdada = "Llum";
        if(rbAigua.isChecked())
            tipusdada = "Aigua";
        ClaseUtilidad.InfluxTipusDada = tipusdada;
        gbMqtt.publishMessage("EnviamDadesLlum", datainici + ' ' + datafinal + ' '+ tipusdada);
        finish();
    }


    @Override
    public void onSuccess(IMqttToken asyncActionToken)
    {

    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {

    }

}
