<!DOCTYPE html>
<html>
<head>
<h1>GossetBo.e</h1>
</head>
<body>
<h3>Noves prestacions</h3>
Aquest projecte, és una ampliació del meu projecte anterior <b>GossetBo</b>.
La versió anterior dona prestacions pel control d'accés domestic.
Aquesta nova versió, a més de les prestacions de l'anterior, proporciona un gràfic del consum d'energia eléctrica de la llar en un lapse de temps seleccionat pel usuari.
<h3>Us de sensor SCT-013</h3>
En aquesta versió s'ha afegit un altre placa ESP32, que llegeix les dades d'un sensor de current SCT-013.
Aquesta nova placa, utilitza el programari a la carpeta: <b>GbCurrent</b> del repositori.
<h3>Noves classes</h3>
Tant el programari de Python per a la Raspberry, com el de Java pel mòbil Android, s'han ampliat amb noves classes.
<h3>Nou manual</h3>
El nou manual <b>Manual_GossetBo.e</b> recull explicacions de tot el referent al nou maquinari i programari, però el manual anterior <b>Manual_GossetBo</b>, continua sent convenient per conèixer tot el que ja estava implementat a la versió anterior. Per això l'he inclòs també en aquest projecte.
</body>
</html>

