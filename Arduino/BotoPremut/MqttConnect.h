

void setupMqtt();
void mqttConnect(void * parameter);
void incomingSub(char* topic, byte* payload, unsigned int length);
void incrementaIP();
void publicaMensaje(char * topic, char * msg);
void setMqtt_server(String ip);
void setMqtt_port(String port);
void setMqtt_user(String user);
void setMqtt_pwd(String pwd);
void setMqtt_topic(String topic);
char * getTopic();

int estatConexio();
