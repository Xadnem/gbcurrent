/****************************************************************************
 * 
 * MODULO PARA REALIZAR OPERACIONES SOBRE LOS FICHEROS EN LA TARJETA SD
 * 
 * 
 ****************************************************************************/


#include <ArduinoJson.h>
#include "SD.h"
#include "SPI.h"
#include "GestorFicheros.h"

StaticJsonDocument<1000> doc;

////////////////////////
//
// COMPRUEBA LA INICIALIZACION CORRECTA
// DE LA TARJETA
//
//////////////////////////////
//
bool compruebaTarjeta(int pin)
{
  
  if(SD.begin(pin)){
    return true;
  }
 return false;
}

////////////////////////
//
// LEE EL CONTENIDO DEL FICHERO CUYA RUTA
// SE PASA COMO PRIMER ARGUMENTO Y METE SU
// CONTENIDO EN LA ARRAY DE chars PASADA COMO
// SEGUNDO ARGUMENTO CUYO LARGO SE PASA COMO TERCERO
//
//////////////////////////////
//
void leerFichero(char * ruta,char * contenido,int largomax)
{
  File fichero = SD.open(ruta,FILE_READ);
  int puntero = 0;
  if (fichero) {
    // Leer del fichero hasta que no queden bytes
    while (fichero.available()) {
      contenido[puntero] = fichero.read();
      puntero++;
      if(puntero >= largomax)
        break;
    }
    contenido[puntero] = '\0';
    fichero.close();  
  } 
  else {
    Serial.print("error abriendo" );
    Serial.println(ruta);
   }
}

///////////////////////////////
//
//  DESERIALIZA EL JSON EN LA SD Y METE LOS DATOS PARA 
//  LA CONEXION WIFI Y AL BROKER MQTT EN LA LISTA DE CADENAS
//  PASADA COMO ARGUMENTO
//
/////////////////////////////////////////
//
void deserializaJson(String json,const char ** lista)
{  
  // Deserialize the JSON document
  //char json2 [] = "{\"sensor\":\"gps\",\"time\":1351824120 }";
  //Serial.println("JSON");
  //Serial.println(json);
  DeserializationError error = deserializeJson(doc, json);
  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  const char * ssid = doc["SSID"];
  const char * netpwd = doc["NETPWD"];
  const char * broker = doc["BROKER"];
  const char * brokerpwd = doc["BROKERPWD"];
  const char * brokeruser = doc["BROKERUSER"];
  const char * brokerport = doc["BROKERPORT"];
  const char * topic = doc["ESTATBOTO"];
  
  lista[0] = ssid;
  lista[1] = netpwd;
  lista[2] = broker;
  lista[3] = brokerpwd;
  lista[4] = brokeruser;
  lista[5] = brokerport;
  lista[6] = topic;
}
