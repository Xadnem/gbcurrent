from influxdb import InfluxDBClient
import datetime

class GbInflux(InfluxDBClient):
    def __init__(self,database,user,password):
        InfluxDBClient.__init__(self,host = '127.0.0.1',port = 8086, database = database,username = user, password = password)
        self.dades = []
        #self.resulset = None
        self.datainici = "" # cadena amb la data,hora,minut i segon del primer registre
        self.datafi = "" # cadena amb la data,hora,minut i segon del ultim registre
        self.flag = 0 # Serà 1 si les dades son de consum de llum i 2 si son de consum d'aigua
        self.unitatstemporals = "" # Serà s,m,h,d,ms en funcio de si les dades son de segons, minuts, hores, dies o messos
    
    
    def establirResulset(self,inici,fi,flag):
        """ Estableix les unitats temporals en funció del llarg de temps del lapse entre la data d'inici i
            la data final passades als dos primers arguments, fa la consulta a la base de dades a la taula
            segons el tercer argument (flag = 1 consumllum flag = 2 consumaigua ) i omple la llista de dades
            de la classe amb tuples amb el valor i el segon,minut,hora o dia en funció de les unitats temporals."""
        
        # Establir la taula a la que es farà la consulta
        self.flag = flag
        taula="consumaigua"
        if flag == 1:
            taula = "consumllum"

        # Obtenir un resulset i un generator del mateix amb totes les dades entre les dates d'inici i fi    
        query = "select * from "+taula+" where time > \'" + inici + "\' and time < \'" + fi + "\'"
        resulset = self.query(query)
        generator = resulset.get_points(taula)
        llistatotal = list(generator)
        if len(llistatotal) == 0:
            return
        # Obtenir la data,hora,minut i segons del primer i ultim registre i assignar-los als atributs de la classe
        self.datainici = llistatotal[0]['time']
        self.datafi = llistatotal[len(llistatotal)-1]['time']
        # Establir la quantitat de segons entre la data d'inici i la final
        segonslapse = self.segonsInterval()
        # Establir les unitats temporals en segons,minuts,hores,dies o mesos 
        if segonslapse <= 300: # Si el lapse es menor a 5 minuts, expresar en segons
            self.unitatstemporals = 's'
        elif segonslapse > 300 and segonslapse  <= 10800: # Si el lapse es menor a 3 hores, expresar en minuts
            self.unitatstemporals = 'm'
        elif segonslapse > 10800 and segonslapse  <= 259200: # Si el lapse es menor a 3 dies, expresar en hores
            self.unitatstemporals = 'h'
        elif segonslapse > 259200 and segonslapse  <= 2592000: # Si el lapse es menor a 1 mes, expresar en dies
            self.unitatstemporals = 'd'
        else:
            self.unitatstemporals = 'ms'
       
       # Omplir la llista amb tuples amb el valor i el temps en funció de les unitats temporals
        contador = 0
        sumawatts = 0
        tempsant = 0
        temps = 0
        if self.unitatstemporals == 's':
            for d in llistatotal:                
                timestamp = d['time']
                segon = int(GbInflux.obtenirSegons(timestamp))
                watss = d['watts']
                self.dades.append((segon,watss))   
        else:
            for d in llistatotal:
                # Obtenir el minut, hora o dia de la dada segons l'escala temporal
                timestamp = d['time']
                if self.unitatstemporals == 'm':
                    temps = int(GbInflux.obtenirSegons(timestamp))
                elif self.unitatstemporals == 'h':
                    temps = int(GbInflux.obtenirMinuts(timestamp))
                elif self.unitatstemporals == 'd':
                    temps = int(GbInflux.obtenirHores(timestamp))
                # Si no s'ha completat un minut,hora o dia, anar sumant els watts i contar la quantitat de dades
                if tempsant <= temps:
                    sumawatts += d['watts']
                    contador += 1
                    tempsant = temps
                # Quan s'hagi completat un minut,hora o dia
                elif contador > 0:
                    # Obtenir el minut,hora o dia de la dada
                    if self.unitatstemporals == 'm':
                        temps = int(GbInflux.obtenirMinuts(timestamp))
                    elif self.unitatstemporals == 'h':
                        temps = int(GbInflux.obtenirHores(timestamp))+2
                        if temps > 23:
                            temps = temps - 24
                    elif self.unitatstemporals == 'd':
                        temps = int(GbInflux.obtenirDies(timestamp))
                    # Ficar la tupla amb el minut,hora o dia i la mitjana dels watts en aquesta unitat temporal a la llista
                    # de dades de la classe.
                    self.dades.append((temps,sumawatts/contador))
                    contador = 0
                    sumawatts = 0
                    tempsant = 0
       
                    
    def mitjaWatts(self,):
        sumawatts = sum([v for (t,v) in self.dades])
        return sumawatts / len(self.dades)


    def segonsInterval(self,):
        """ Retorna la quantitat de segons entre l'inici i el final del interval de temps del resulset del objecte """
        epocini = GbInflux.timestampToSeconds(self.datainici)
        epocfi = GbInflux.timestampToSeconds(self.datafi)
        return(epocfi-epocini)

    def valorMax(self, ):
        """ Retorna la tupla del valor màxim dintre de la llista de dades de l'objecte """
        valors = [v[1] for v in self.dades]
        maxim = max(valors)
        return self.dades[valors.index(maxim)]

    def KwhInterval(self, ):
        """ Retorna els KWh en el inteval del resulset de l'objecte """
        return (self.mitjaWatts() / 1000 * (self.segonsInterval() / 3600))

    def getNomUnitats(self, ):
        """ Retorna les unitats temporals que s'utilitzen a la llista de dades del objecte
            amb la paraula complerta. """
        if self.unitatstemporals == 's':
            return "Segons"
        elif self.unitatstemporals == 'm':
            return "Minuts"
        elif self.unitatstemporals == 'h':
            return "Hores"
        elif self.unitatstemporals == 'd':
            return "Dies"
        elif self.unitatstemporals == 'ms':
            return "Mesos"
        else:
            return "Null"



    @classmethod
    def timestampToSeconds(cls,timestamp):
        """ Converteix el timestamp d'InfluxDB en segons de temps unix i els retorna com a nombre enter """
        # Obtenir l'any, mes i dia
        date = timestamp[:10]
        ldate = date.split('-')
        y,m,d = ldate[0],ldate[1],ldate[2]
        # Obtenir hora, minuts i segons
        hora = timestamp[11:13]
        minut = timestamp[14:16]
        segon = timestamp[17:19]
        # Convertir la data de inici en segons des de epoc
        epoc = datetime.datetime(int(y),int(m),int(d),int(hora),int(minut),int(segon)).strftime("%s")
        return int(epoc)
    
    @classmethod
    def obtenirSegons(cls,timestamp):
        l = timestamp.split(':')
        return l[2][0:2]
    
    @classmethod
    def obtenirMinuts(cls,timestamp):
        l = timestamp.split(':')
        return l[1]
    
    @classmethod
    def obtenirHores(cls,timestamp):
        l = timestamp.split(':')
        return l[0][-2:]
    
    @classmethod
    def obtenirDies(cls,timestamp):
        l = timestamp.split(':')
        return l[0][8:10]
        
    @classmethod
    def timestampAUTC(cls,timestamp):
        """ RESTA DUES HORES AL L'HORA DEL TIMESTAMP PASSAT COM A ARGUMENT PER CONVERTIR-LO
            DE L'HORA DE CATALUNYA A HORA UTC """
        
        horautc = int(timestamp[11:13])-2
        if horautc < 0:
            horautc += 24
        horautcs = str(horautc)
        if len(horautcs)==1:
            horautcs = "0"+horautcs
        timestamp = timestamp[0:11]+horautcs+timestamp[13:]
        return timestamp

    @classmethod
    def construeixTimeStamp(cls,data, hora, minut, segon):
        """ RETORNA UN TIMESTAMP EN HORA UTC AMB LA HORA UTC+2 ( HORA DE CATALUNYA ), AMB LA
            DATA, HORA, MINUT I SEGONS PASSATS COM A ARGUMENT """
        if len(hora) == 0:
            hora = "00"
        if len(minut) == 0:
            minut = "00"
        if len(segon) == 0:
            segon = "00"
        timestamp = data + "T" + hora + ":" + minut + ":" + segon + "Z"
        return GbInflux.timestampAUTC(timestamp)
    
    
