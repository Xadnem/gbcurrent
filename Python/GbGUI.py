#!/usr/bin/python3.8

import tkinter
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import subprocess
from GbSecretTopic import *
from tkcalendar import *
import datetime
from os import system,path
from GbMqtt import GbMqtt
from GbInflux import GbInflux
from GbGrafic import GbGrafic


class GbGUI:

    def __init__(self):
        
        # Ventana gràfica per encabir la resta de ginys
        self.root = tkinter.Tk()
        self.alto, self.ancho = self.root.winfo_screenheight(), self.root.winfo_screenwidth()
        self.root.title("Control de GossetBo")
        self.root.geometry("%dx%d+0+0" % (self.ancho, self.alto))
        rutaicono =   path.dirname(path.realpath(__file__)) + path.sep + 'GossetBo.ico'
        

        # Definicions de tipus de lletra i colors
        altofontRetols = int(self.alto * 0.013)
        altofontRetolsGran = int(self.alto * 0.017)
        altofontAnotacions = int(self.alto * 0.008)
        fons = "SeaGreen"
        fonscapses = "LightGreen"
        fontRetolsGran = ("Arial", altofontRetolsGran, "underline")
        fontRetols = ("Arial", altofontRetols, "underline")
        fontText = ("Arial", altofontRetols)
        fontAnotacions = ("Arial", altofontAnotacions)
        self.root.configure(bg=fons)

        # Unitats en funció del alt i ample de la pantall per a situar els widgets
        unialto = int(self.alto * 0.01)
        # Etiqueta retol de l'estat del sistema de control
        retolControl = Label(self.root, text='Estat del sistema')
        retolControl.configure(font=fontRetols, bg=fons)
        retolControl.place(x=25, y=unialto * 2)

        # Etiqueta per a mostrar l'estat del sistema de control
        self.lbEstat = Label(self.root, text='Sistema desactivat')
        self.lbEstat.pack()
        self.lbEstat.place(x=25, y=unialto * 6, height=altofontRetols * 2, width=self.ancho * 0.15)
        self.lbEstat.configure(font=fontText)

        # Etiqueta per a mostrar l'estat de conexió
        self.lbConexio = Label(self.root, text='')
        self.lbConexio.place(x=self.ancho - self.ancho * 0.1 - 20, y=unialto * 2, height=altofontRetols * 2, width=self.ancho * 0.1)

        # Etiqueta per al retol de canvi de codi de desactivació
        retolCanviCodi = Label(self.root, text='Canvi del codi de desactivació')
        retolCanviCodi.configure(font=fontRetols, bg=fons)
        retolCanviCodi.place(x=15, y=unialto * 15)

        # Capsa de text per a introduir el codi actual
        self.tbCodiActual = Entry(self.root);
        self.tbCodiActual.configure(font=fontText, bg=fonscapses)
        anchocapsa = self.ancho * 0.15
        self.tbCodiActual.place(x=15, y=unialto * 19, width=anchocapsa)

        # Etiqueta per a l'anotació del codi actual
        anotacioCodiActual = Label(self.root, text='Codi actual')
        anotacioCodiActual.configure(font=fontAnotacions, bg=fons)
        anotacioCodiActual.place(x=15 + anchocapsa + 10, y=unialto * 19)

        # Capsa de text per a introduir el nou codi
        self.tbNouCodi = Entry(self.root)
        self.tbNouCodi.configure(font=fontText, bg=fonscapses)
        self.tbNouCodi.place(x=15, y=unialto * 25, width=anchocapsa)

        # Etiqueta per a l'anotació del nou codi
        anotacioNouCodi = Label(self.root, text='Nou codi')
        anotacioNouCodi.configure(font=fontAnotacions, bg=fons)
        anotacioNouCodi.place(x=15 + anchocapsa + 10, y=unialto * 25)

        # Botó per a aceptar el nou codi
        btCanviCodi = Button(self.root, text="Acceptar", bg="GreenYellow", command=self.onBtCanviCodi_Clicked)
        btCanviCodi.place(x= anchocapsa/2 -30, y=unialto * 30)

        # Frame per a l'area de presa de dades pel gràfic
        frame = Frame(self.root,bg = "lightseagreen")
        anchoframe = self.ancho/6
        frame.place(x = 20, y=unialto * 37, width = anchoframe, height = self.alto/2.8)

        # Etiqueta pel retol del àrea de presa de dades pel gràfic
        lbRetolGrafana = Label(frame, text="Gràfic", bg="lightseagreen", fg="Black",
                               justify="left", anchor=W, font=fontRetolsGran)
        lbRetolGrafana.place(x=(anchoframe/2)-30, y=unialto/2)
        
        # Frame pels widgets de presa de dades del periode d'inici
        frameinici = Frame(frame,bg = "green")
        frameinici.place(x = 15, y = unialto*5, width = anchoframe-30, height = self.alto/9)
        
        # Etiqueta pel retol del periode d'inici
        lbDataInici = Label(frameinici, text="Dades d'inici de periode", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontRetols)
        lbDataInici.place(x=10 , y=unialto)
        
        # Etiqueta pel retol del calendari d'inici
        anchocal = int(self.ancho * 0.06)
        lbCalInici = Label(frameinici, text="Data", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        lbCalInici.place(x=anchocal/2 , y=unialto*4.5)
        
        # EntryCalendar per a la data d'inici
        self.calendariinici = DateEntry(frameinici, font=fontText)
        self.calendariinici.place(x=10, y=unialto * 6.7, width=anchocal)
        
        # Etiqueta pel retol de l'hora d'inici
        lbHoraInici = Label(frameinici, text="Hora", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        lbHoraInici.place(x=(anchoframe/2.5)+4 , y=unialto*4.5)
        
        #ComboBox per a l'hora d'inici de periode
        self.cbHoraInici = ttk.Combobox(frameinici,font = fontText)
        self.cbHoraInici.place(x = anchoframe/2.5,y = unialto*6.7, width = anchoframe/7)
        self.cbHoraInici['values']=( tuple("%02d".rjust(2,'0') % x for x in range(24)))
        
        # Etiqueta pel retol del minut d'inici
        lbMinutInici = Label(frameinici, text="Minut", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        lbMinutInici.place(x=(anchoframe/1.81)+4 , y=unialto*4.5)
        
        #ComboBox pel minut d'inici de periode
        self.cbMinutInici = ttk.Combobox(frameinici,font = fontText)
        self.cbMinutInici.place(x = anchoframe/1.81,y = unialto*6.7, width = anchoframe/7)
        self.cbMinutInici['values']=( tuple("%02d".rjust(2,'0') % x for x in range(60)))
        
        # Etiqueta pel retol del segon d'inici
        lbSegonInici = Label(frameinici, text="Segon", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        lbSegonInici.place(x=(anchoframe/1.42) , y=unialto*4.5)
        
        #ComboBox pel segon d'inici de periode
        self.cbSegonInici = ttk.Combobox(frameinici,font = fontText)
        self.cbSegonInici.place(x = anchoframe/1.42,y = unialto*6.7, width = anchoframe/7)
        self.cbSegonInici['values']=( tuple("%02d".rjust(2,'0') % x for x in range(60)))
        
        # Frame pels widgets de presa de dades del periode final
        framefi = Frame(frame,bg = "green")
        framefi.place(x = 15, y = unialto*18, width = anchoframe-30, height = self.alto/9)
        
        # Etiqueta pel retol del periode d'inici
        lbDataFi = Label(framefi, text="Dades d'final de periode", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontRetols)
        lbDataFi.place(x=10 , y=unialto)
        
        # Etiqueta pel retol del calendari d'inici
        anchocal = int(self.ancho * 0.06)
        lbCalFi = Label(framefi, text="Data", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        lbCalFi.place(x=anchocal/2 , y=unialto*4.5)
        
        # Mostrar el EntryCalendar per a la data de fi del periode
        self.calendarifi = DateEntry(framefi, font=fontText)
        self.calendarifi.place(x=10, y=unialto * 6.7, width=anchocal)
        
        # Etiqueta pel retol de l'hora de fi
        lbHoraFi = Label(framefi, text="Hora", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        lbHoraFi.place(x=(anchoframe/2.5)+4 , y=unialto*4.5)
        
        #ComboBox per a l'hora de fi
        self.cbHoraFi = ttk.Combobox(framefi,font = fontText)
        self.cbHoraFi.place(x = anchoframe/2.5,y = unialto*6.7, width = anchoframe/7)        
        self.cbHoraFi['values']=( tuple("%02d".rjust(2,'0') % x for x in range(24)))
        
        # Etiqueta pel retol del minut de fi
        lbMinutFi = Label(framefi, text="Minut", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        lbMinutFi.place(x=(anchoframe/1.81)+4 , y=unialto*4.5)
        
        #ComboBox pel minut de fi de periode
        self.cbMinutFi = ttk.Combobox(framefi,font = fontText)
        self.cbMinutFi.place(x = anchoframe/1.81,y = unialto*6.7, width = anchoframe/7)
        self.cbMinutFi['values']=( tuple("%02d".rjust(2,'0') % x for x in range(60)))
        
        # Etiqueta pel retol del segon de fi
        lbSegonFi = Label(framefi, text="Segon", bg="green", fg="Black",
                            justify="left", anchor=W, font=fontAnotacions)
        lbSegonFi.place(x=(anchoframe/1.42) , y=unialto*4.5)
        
        #ComboBox pel segon d'inici de periode
        self.cbSegonFi = ttk.Combobox(framefi,font = fontText)
        self.cbSegonFi.place(x = anchoframe/1.42,y = unialto*6.7, width = anchoframe/7)
        self.cbSegonFi['values']=( tuple("%02d".rjust(2,'0') % x for x in range(60)))
        
        # Botó per obrir el gràfic de consums
        anchoboton = int(self.ancho * 0.075)
        btMostrarGrafic = Button(frame, text="Mostrar Gràfic", bg="RoyalBlue", command=self.onBtMostrarGrafic_Clicked)
        btMostrarGrafic.place(x=anchoframe - anchoboton - 15, y=unialto * 31, width=anchoboton)
        
        # Radio buttons per seleccionar llum o aigua
        self.seleccio = IntVar()
        self.rbLlum = Radiobutton(frame,text = "Llum",variable = self.seleccio,value = 1, bg = "lightseagreen",font = fontAnotacions)
        self.rbLlum.place(x = 10, y = unialto * 31, width = anchoboton/2)
        self.rbLlum.select()
        self.rbAigua = Radiobutton(frame,text = "Aigua",variable = self.seleccio,value = 2, bg = "lightseagreen",font = fontAnotacions)
        self.rbAigua.place(x = anchoboton/2+15, y = unialto * 31, width = anchoboton/2)
        
        
    def onBtCanviCodi_Clicked(self):

        """ Comprova que s'han omplert les dues capses de text del codi actual i el nou. Si el canvi es porta a terme
         es mostra un missatge i es publica en el broker per a ser canviat a tots els dispositius mobils associats."""
        if self.tbCodiActual.get() != GbSecretTopic.codi:
            messagebox.showerror("Error", "Codi actual erroni")
        elif len(self.tbNouCodi.get()) == 0:
            messagebox.showerror("Error", "El nou codi no pot estar buit")
        else:
            codi = self.tbNouCodi.get()
            canviat = GbSecretTopic.canviarCodi(codi)
            if canviat:
                messagebox.showinfo("OK", "Codi de desactivació canviat")
                self.tbCodiActual.delete(0, len(self.tbCodiActual.get()))
                self.tbNouCodi.delete(0, len(self.tbNouCodi.get()))
                GbMqtt.publicaEnBroker(GbSecretTopic.codicanviat,codi)
            else:
                messagebox.showerror("Error", "No s'ha canviat el codi de desactivació." + GbSecretTopic.codi)

        

    def onBtMostrarGrafic_Clicked(self):
        """ CALLBACK DEL BOTO btMostrarGrafic QUE PORTA A TERME LES OPERACIONS NECESSARIES PER A MOSTRAR
            EL GRAFIC DE CONSUM AMB LES DADES SELECCIONADES PEL USUARI """
        
        # Construir les cadenes amb les datas, hora, minut i segons d'inici i final
        inici = str(self.calendariinici.get_date()) # Data d'inici
        inicih = self.cbHoraInici.get() # Hora d'inici        
        inicimin = self.cbMinutInici.get()# Minuts d'inici        
        iniciseg = self.cbSegonInici.get()
        cadenainici = GbInflux.construeixTimeStamp(inici,inicih,inicimin,iniciseg)
        fi = str(self.calendarifi.get_date())
        fih = self.cbHoraFi.get()
        fimin = self.cbMinutFi.get()
        fiseg = self.cbSegonFi.get()        
        cadenafi = GbInflux.construeixTimeStamp(fi,fih,fimin,fiseg)
        # Crear un objecte GbInflux amb les dades de connexió a la base de dades desades a GbSecretTopic
        connection = GbInflux(GbSecretTopic.nombd,GbSecretTopic.usuaribd,GbSecretTopic.pwdbd)
        # Omplir l'atribut resulset del objecte GbInflux amb la consulta segons les dates,hora,minut i segons seleccionats
        connection.establirResulset(cadenainici,cadenafi,self.seleccio.get())
        # Si no hi ha resultats, mostrar missatge d'error        
        if len(connection.dades) == 0:
            messagebox.showerror("Error", "No hi ha resultats en el lapse seleccionat.")
            return
        # Frame per a la finestra del gràfic
        framegrafic = Frame(self.root,bg = "green")
        anchoframe = self.ancho/1.3
        altoframe = self.alto/1.3
        framegrafic.place(x = self.ancho/4.8, y = self.alto/6.5, width = anchoframe, height = altoframe)
        #Mostrar el gràfic
        GbGrafic(connection,anchoframe,altoframe,framegrafic)
        #Mostrar el consum en Kwh del lapse graficat
        kwh = connection.KwhInterval()
        lbResultat = Label(framegrafic, text="Kwh:" + '%.6f' % kwh , bg="green", fg="Black",
                            justify="left", anchor=W, font=("Arial",17))
        lbResultat.place(x=5,y=altoframe-50)
        

        
