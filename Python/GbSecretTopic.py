import configparser


class GbSecretTopic:
    # Instanciació dels atributs de la classe
    codi = username = pwd = broker = subsboto = subenviamentcodi = subsonoff = estatboto = codienviat = \
        codiok = sonoff = activacamara = comunicaestat = codicanviat = consultacodi = nombd = \
        usuaribd = pwdbd = ""
    ruta =''
    port = ta = 0

    @classmethod
    def establirSecrets(cls, ):

        """ Llegeix el fitxer de configuració confing.ini i assigna els valors al atributs de la classe """
        config = configparser.ConfigParser()
        config.read(GbSecretTopic.ruta)
        GbSecretTopic.codi = config['GBCONTROL']['CODI']
        GbSecretTopic.username = config['MQTT']['USER']
        GbSecretTopic.pwd = config['MQTT']['PWD']
        GbSecretTopic.broker = config['MQTT']['BROKER']
        GbSecretTopic.port = int(config['MQTT']['PORT'])
        GbSecretTopic.ta = int(config['MQTT']['KEEPALIVE'])
        GbSecretTopic.subsboto = config['SUBCRIPTIONS']['BOTO']
        GbSecretTopic.subenviamentcodi = config['SUBCRIPTIONS']['ENVIAMENTCODI']
        GbSecretTopic.subsonoff = config['SUBCRIPTIONS']['SONOFF']
        GbSecretTopic.estatboto = config['SUBCRIPTIONS']['BOTO']
        GbSecretTopic.codienviat = config['SUBCRIPTIONS']['ENVIAMENTCODI']
        GbSecretTopic.codiok = config['SUBCRIPTIONS']['CONFIRMACODI']
        GbSecretTopic.sonoff = config['SUBCRIPTIONS']['SONOFF']
        GbSecretTopic.activacamara = config['SUBCRIPTIONS']['ACTIVACAMARA']
        GbSecretTopic.comunicaestat = config['SUBCRIPTIONS']['COMUNICAESTAT']
        GbSecretTopic.codicanviat = config['SUBCRIPTIONS']['CODICANVIAT']
        GbSecretTopic.consultacodi = config['SUBCRIPTIONS']['CHECKCODI']
        GbSecretTopic.nombd = config['INFLUX']['DATABASE']
        GbSecretTopic.usuaribd = config['INFLUX']['USER']
        GbSecretTopic.pwdbd = config['INFLUX']['PASSWORD']

    @classmethod
    def canviarCodi(cls, noucodi):

        """ Canvia el codi de desactivació al fitxer de configuració i el valor del atribut de la classe """
        config = configparser.ConfigParser()
        config.read(GbSecretTopic.ruta)
        config.set('GBCONTROL', 'CODI', noucodi)
        try:
            with open(GbSecretTopic.ruta, 'w') as configfile:
                config.write(configfile)
            GbSecretTopic.codi = noucodi
        except Exception as e:
            print(str(e))
            return False
        return True
