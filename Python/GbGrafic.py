from tkinter import *
from math import floor,log


class GbGrafic:
    def __init__(self,gbinflux,ample,alt,frame):
        self.gbinflux = gbinflux
        self.ample = ample
        self.alt = alt
        self.escalav = 0  # Pixels per valor al eix de les ordenades
        self.escalah = 0  # Pixels per valor al eix d'abcises
        # Etiqueta amb el titol
        altofontRetols = int(alt*0.013)
        fontRetols = ("Arial", altofontRetols, "underline", "bold")
        retol = Label(frame, text = "Gràfic de GossetBo",font = fontRetols,bg = "green")
        retol.place(x = self.ample/2-20, y=2)
        #Canvas per a dibuixar
        self.cvLienzo = Canvas(frame,width = self.ample-20, height = self.alt-100,background = "black" )
        self.cvLienzo.place(x = 10, y = 30)
        # Inici i fi dels eixos vertical i horizontal
        self.iniciov = self.alt - 200
        self.inicioh = 120
        self.finh = self.ample - 50
        self.finv = 60
        self.dibuixarGrafic()
        
        

    def dibuixarGrafic(self):
        """ DIBUIXA EL GRAFIC SEGONS LES DADES AL OBJECTE GbInflux DE LA CLASSE """

        # Dibuixar l'eix d'ordenades ( y )
        self.cvLienzo.create_line(self.inicioh,self.iniciov, self.inicioh, self.finv,fill = "white",width = 5)
        # Dibuixar l'eix d'abcises
        self.cvLienzo.create_line(self.inicioh, self.iniciov, self.finh, self.iniciov, fill="white", width=5)
        # Obtenir el valor maxim a graficar a l'eix d'ordenades
        ymax = self.gbinflux.valorMax()[1]        
        # Establir els pixels per unitat a l'eix d'ordenades
        if ymax > 0:
            self.escalav = (self.iniciov - self.finv)/ymax
        else:
            self.escalav = (self.iniciov - self.finv)
        # Establir els pixels per unitat a l'eix d'abcises
        self.escalah = (self.finh - self.inicioh) / (len(self.gbinflux.dades)-1)
        # Dibuixar les linies de divisió del eix d'ordenades
        self.dibuixarDivisionsVerticals(ymax)
        # Mostrar la data i hora d'inici i final del lapse a graficar
        self.mostrarDates()
        # Dibuixar les datas d'inici i final , l'escala temporal del eix d'abcises i la linia del gràfic
        self.dibuixarLiniesAbcises()
        # Mostrar el rétol amb les unitats temporals del gràfic
        self.mostrarUnitatsTemporals()
        # Mostrar el màxim
        self.mostrarMaxim()
        # Mostrar la mitja del consum en el lapse
        self.mostrarMitja()
        # Mostrar el valor actual
        self.mostrarActual()
        
    
    def dibuixarDivisionsVerticals(self,rangov):
        """ DIBUIXA LES LINIES DE DIVISIO EN EL EIX VERTICAL AMB UN ESPACIAT EN FUNCIO DEL RANG PASSAT COM ARGUMENT """
        
        # Establir l'interval vertical entre les linies de divisio en funcion del rang de valors
        if rangov <= 50:
            espaciadov = self.escalav  # una linia per cada unitat del valor a graficar
        elif rangov <= 100:
            espaciadov = 10 * self.escalav
        elif rangov <= 300:
            espaciadov = 20 * self.escalav
        elif rangov <= 400:
            espaciadov = 25 * self.escalav
        elif rangov <= 500:
            espaciadov = 30 * self.escalav
        elif rangov <= 750:
            espaciadov = 50 * self.escalav
        elif rangov <= 1000:
            espaciadov = 70 * self.escalav
        elif rangov <= 1500:
            espaciadov = 100 * self.escalav
        else:
            espaciadov = 150 * self.escalav  # una linia cada 150 unitats del valor a graficar
        # Establir l'inici i final de les linies de divisió, el color i el tipus de linia
        iniciodv = self.inicioh - 30 # inici i fi de cada linia de divisio vertical
        findv = self.ample - 30
        colorv = "cyan"
        dash = [1,10,]
        # Dibuixar les unitats
        if self.gbinflux.flag == 1:
            unitatsv = "Watts"
        else:
            unitatsv = "Llitres"
        self.cvLienzo.create_text(20, ((self.iniciov-self.finv)/2)+30, text = unitatsv,
                                      fill=colorv, font="Dejavu 13",angle = 90)
        #  Dibuixar les linies de divisio del eix vertical i les legendes dels valors
        contador = 1
        while contador * espaciadov < self.iniciov-self.finv:
            valor = espaciadov / self.escalav * contador  # valor que aparecerá al lado de la linea
            desplazamiento = 0
            desplazamiento = floor(log(valor))
            self.cvLienzo.create_line(iniciodv, self.iniciov-contador * espaciadov, findv,
                                      self.iniciov-contador * espaciadov, fill=colorv,dash = dash )
            contador += 1
            self.cvLienzo.create_text(70 - desplazamiento, self.iniciov - contador * espaciadov, text=str(floor(valor)),
                                      fill=colorv, font="Dejavu 13")
        

    def mostrarDates(self,):
        
        """ MOSTRA LA DATA I HORA D'INICI I FINAL DEL LAPSE QUE S'ESTÀ GRAFICANT """
        fontescala = "Dejavu 7"
        # Mostrar la data i hora d'inici
        stampinici = self.gbinflux.datainici
        stampfi = self.gbinflux.datafi
        datahoraini = GbGrafic.construirData(stampinici)
        datahorafi =  GbGrafic.construirData(stampfi)
        self.cvLienzo.create_text(self.inicioh,self.iniciov+55,text = datahoraini,angle = 0,font = fontescala,fill = "cyan")
        self.cvLienzo.create_text(self.finh-15,self.iniciov+55,text = datahorafi,angle = 0,font = fontescala,fill = "cyan")
    
    @classmethod
    def construirData(cls,timestamp):
        
        """ RETORNA UNA CADENA AMB LA DATA I LA HORA CORRESPONENT AL TIMESTAMP D'INFLUXDB PASSAT COM A ARGUMENT AMB
            L'HORA UTC+2 I LA DATA EN FORMAT DE CATALUNYA """
        # Obtindre la part del timestamp amb la data i formatar-la amb dia, mes i any
        data = timestamp[:10]
        llistadata = data.split('-')
        data = llistadata[2]+'-'+llistadata[1]+'-'+llistadata[0]
        # Obtindre la part del timestamp amb la hora i convertir-la a UTC+2
        hora = timestamp[11:19]
        horafimas = int(hora[0:2])+2
        if horafimas > 23:
            horafimas = horafimas % 24
        horafi = str(horafimas) + hora[2:]
        # Formatar l'hora en cas de que aquesta sigui en punt
        llistahora = horafi.split(':') 
        if llistahora[1] == "59":
            llistahora[1] = "00"
            llistahora[2] = "00"
            if int(llistahora[0]) < 23:
                llistahora[0] = str(int(llistahora[0])+1)
            else:
                llistahora[0] = "00"
                llistadata[2] = str(int(llistadata[2])+1)
                data = llistadata[2]+'-'+llistadata[1]+'-'+llistadata[0]
        # Construir la cadena final
        hora = llistahora[0]+':'+llistahora[1]+':'+llistahora[2]
        return data + '\n'+ hora


    def dibuixarLiniesAbcises(self,):
        """ DIBUIXA LA ESCALA TEMPORAL DEL EIX D'ABCISES I LES LINIES DEL GRAFIC """
        
        fontescala = "Dejavu 7"
        # Instanciació de variables necessaries
        situacionh = self.inicioh
        inicilinia = self.iniciov-10
        finlinia = self.iniciov+10
        color = "white"
        ancholinea = 1
        dataant = 0
        # Dibuixar les linies de divisió de l'escala temporal al eix d'abcises , la linia del gràfic i els nombres
        puntinix = self.inicioh
        puntiniy = self.iniciov-self.gbinflux.dades[0][1] * self.escalav
        for dada in self.gbinflux.dades:
            puntfinx = situacionh
            puntfiny = self.iniciov - dada[1]*self.escalav
            data = dada[0]
            if data < dataant:
                color = "chartreuse"
                ancholinea = 2
            self.cvLienzo.create_line(situacionh, inicilinia, situacionh,finlinia, fill=color,width = ancholinea)
            self.cvLienzo.create_line(puntinix,puntiniy,puntfinx,puntfiny,fill="red",width = 5)
            self.cvLienzo.create_text(situacionh, finlinia + 15, text=str(data), angle=90, font=fontescala, fill=color)
            puntinix = puntfinx
            puntiniy = puntfiny
            situacionh += self.escalah
            color = "white"
            ancholinea = 1
            dataant = data
        
       
        
    def mostrarUnitatsTemporals(self,):
        
        """  MOSTRA EL RETOL AMB LES UNITATS TEMPORALS"""

        retolunitats = self.gbinflux.getNomUnitats()
        self.cvLienzo.create_text(((self.finh-self.inicioh)/2)+50,self.iniciov+90,text = retolunitats,font = "Dejavu 13",fill = "cyan")


    def mostrarMaxim(self,):
        
        """ MOSTRA EL MAXIM EN EL LAPSE GRAFICAT """

        tuplavalormax = self.gbinflux.valorMax()
        index = self.gbinflux.dades.index(tuplavalormax)
        situaciovmax = self.inicioh+(index*self.escalah)
        self.cvLienzo.create_oval(situaciovmax-3.5,(self.iniciov - tuplavalormax[1]* self.escalav)-3.5,
                                  situaciovmax+3.5,(self.iniciov - tuplavalormax[1]* self.escalav)+3.5,fill = "yellow")
        self.cvLienzo.create_text(situaciovmax,(self.iniciov - tuplavalormax[1]* self.escalav)-25,
                                  text = "Max: " + "%.2f" % tuplavalormax[1], font = "Dejavu 13",fill = "yellow")
    
        
    def mostrarMitja(self,):
        
        """ MOSTRA UNA LINIA AMB LA MITJA DEL CONSUM EN EL LAPSE GRAFICAT """
        mitja = self.gbinflux.mitjaWatts()
        self.cvLienzo.create_line(self.inicioh, self.iniciov - mitja * self.escalav,self.finh ,self.iniciov - mitja*self.escalav,
                                  fill="orange",dash = (2,2))
        self.cvLienzo.create_text(self.finh-80,self.iniciov - mitja * self.escalav - 15,
                                  text = "Mitja: %.2f" % mitja, font = "Dejavu 11",fill = "orange")
        

    def mostrarActual(self,):
        
        """ MOSTRA EL VALOR ACTUAL """
        index = len(self.gbinflux.dades)-1
        tuplavaloractual = self.gbinflux.dades[index]
        situaciovactual = self.inicioh+(index*self.escalah)
        self.cvLienzo.create_oval(situaciovactual-3.5,(self.iniciov - tuplavaloractual[1]* self.escalav)-3.5,
                                  situaciovactual+3.5,(self.iniciov - tuplavaloractual[1]* self.escalav)+3.5,fill = "lightgreen")
        self.cvLienzo.create_text(situaciovactual-45,(self.iniciov - tuplavaloractual[1]* self.escalav)+25,
                                  text = "últim: " + "%.2f" % tuplavaloractual[1], font = "Dejavu 11",fill = "lightgreen")
