import paho.mqtt.client as mqtt


class GbMqtt(mqtt.Client):

    broker = None # Objtecte static de la clase per utilitzar a altres clases

    def __init__(self, llistaTopics):
        mqtt.Client.__init__(self)
        self.llistaTopics = llistaTopics

    def establirUserPassword(self, user, pwd):
        self.username_pw_set(user, pwd)

    def connectarABroker(self, broker, port, timealive):
        self.connect(broker, port, timealive)

    def subscriureTopics(self):
        for topic in self.llistaTopics:
            self.subscribe(topic)

    @classmethod
    def publicaEnBroker(cls,topic,msg):
        cls.broker.publish(topic,msg)
        print("Publicant amb Topic" + topic + " Missatge: " + msg)

