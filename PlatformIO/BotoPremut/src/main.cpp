#include <Arduino.h>

#include "GestorFicheros.h"
#include "WifiConnect.h"
#include "MqttConnect.h"

#define pin 5
#define largofichero 1000
#define pinProximitySensor 34

char * configuracion;
int estatSensor;
void publicaEstatSensor(void * parameter);
int contador = 0;
int cuentaceros = 0;
int cuentaunos = 0;
//Ciclos en los que se reinicia la placa para 
// evitar bloqueo por digital read repetidos
const int CICLOSMAX = 6000;


void setup() {
   pinMode(pinProximitySensor,INPUT);
   Serial.begin(115200);
   
   /* Comprobracion y apertura del stream a la tarjeta SD */
   if(compruebaTarjeta(pin))
    Serial.println("Tarjeta OK");
   else
    Serial.println("Error al abrir la tarjeta.");
    
    /* Obtener el String correspondiente al Json con los datos de configuracion
     *  en la tarjeta  */
    configuracion = (char*)malloc(largofichero);
    leerFichero("/Datos.txt",configuracion,largofichero);
    String configuracions =(String)configuracion;
    //Serial.println("Datos.txt");
    //Serial.println(configuracion);    
    //Serial.println("Config " + configuracions);
    
    /* Deserializar el json y meter los datos en una array de cadenas */
    const char ** lista = (const char**)malloc(sizeof(char*)*10);
    deserializaJson(configuracions,lista); 
    //Serial.println("DATOS:");
    //for(int i = 0; i < 6;i++)
      //Serial.println(lista[i]);  
      
    /* Establecer el valor de los datos de conexion a la red wifi */
    setSsid(lista[0]);
    setPwd(lista[1]);
    
    /* Establecer el valor de los datos de conexion al broker */
    setMqtt_server(lista[2]);
    setMqtt_port(lista[5]);
    setMqtt_user(lista[4]);
    setMqtt_pwd(lista[3]);  
    setMqtt_topic(lista[6]);
    
    
    /* Declaracion de tareas de FreeRTOS */   
    xTaskCreate(
                    conectaWifi,      // Task function. 
                    "conectaWifi",    // String with name of task. 
                    10000,            // Stack size in bytes. 
                    NULL,             // Parameter passed as input of the task 
                    1,                // Priority of the task. 
                    NULL);            // Task handle. 
                   

  /* Establecer los valores iniciales del broker */
    setupMqtt();
    

    xTaskCreate(
          mqttConnect,      // Task function. 
          "mqttConnect",    // String with name of task. 
          10000,            // Stack size in bytes. 
          NULL,             // Parameter passed as input of the task 
          1,                // Priority of the task. 
          NULL);            // Task handle. 

  /* Comprobar el estado del boton */
     xTaskCreate(
          publicaEstatSensor,      // Task function. 
          "publicaEstadoBoton",    // String with name of task. 
          10000,            // Stack size in bytes. 
          NULL,             // Parameter passed as input of the task 
          1,                // Priority of the task. 
          NULL);            // Task handle. 
}


/////////////////////////////////
//
//  COMPRUEBA EL ESTADO DEL BOTON Y CAMBIA EL VALOR
//  DE LA VARIABLE EN CONSECUENCIA
//
//////////////////////////////////////
//
void comprobaSensor()
{
  uint16_t lectura = analogRead(pinProximitySensor);
  Serial.println(lectura);
  //Cuando el sensor no detecta, da una lectura por encima de 500
  // pongo 300 para evitar poner > 0 y evitar valores residuales
  if( lectura > 300)
  {
    cuentaceros++;
    cuentaunos = 0;
    if(cuentaceros == 3)
    {
      estatSensor = 0;
      cuentaceros = 0;
    }
    Serial.println("Sensor no detecta");    
  }
  //Cuando el sensor de proximidad detecta, la mayoría de veces es cero
  // pero puede dar algun valor residual
  else 
  {
    Serial.println("Sensor detectant");
    cuentaceros = 0;
    cuentaunos++;
    if(cuentaunos == 3)
    {
      estatSensor = 1;
      cuentaunos = 0;
    }
  }  
}

///////////////////////////////
// 
// PUBLICA EL ESTADO DEL BOTON EN EL BROKER
//
/////////////////////////////////////////
//
void publicaEstatSensor(void * parameter)
{ 
  while(true)
  { 
    if(estatConexio())//Si el cliente Mqtt esta conectado
    {
      comprobaSensor();
      char estat[2];
      sprintf(estat, "%d", estatSensor);
      // Enviament del missatge amb l'estat del botó al topic d'estat
      publicaMensaje(getTopic(), estat);
       contador++;
       String lecturacontador = "";
       lecturacontador.concat(CICLOSMAX - contador);
       lecturacontador.concat(" ciclos para reboot");
       Serial.println(lecturacontador);
      if(contador == CICLOSMAX){
        Serial.println("Rebooting");
        contador = 0;
        ESP.restart();
      }
      vTaskDelay(1000); 
    }   
  }  
  vTaskDelete( NULL );
}


void loop()
{
  
}
