/****************************************************************************
 * 
 * MODULO PARA REALIZAR LA CONEXION A LA RED WIFI
 * 
 * 
 ****************************************************************************/


#include "WifiConnect.h"

/* Variables globales */
char * ssid;
char * password;

///////////////////////////////
//
// Conexió a la xarxa WiFi
//
////////////////////////////////////////
//
void conectaWifi( void * parameter)
{
  while(true)
  {
    if (WiFi.status() != WL_CONNECTED)
    {
      delay(10);
      Serial.println();
      Serial.print("Conexió a ");
      Serial.println(ssid);
      Serial.print("Amb pwd:  ");
      Serial.println(password);
      WiFi.begin(ssid, password);
     
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        vTaskDelay(10);
      }
      
      randomSeed(micros());
      Serial.println("");
      Serial.println("Conectat a WiFi");
      Serial.println("Direcció IP: ");
      Serial.println(WiFi.localIP());
    }
    vTaskDelay(10);
  }
  vTaskDelete( NULL );
}

///////////////////////
//
//  METE EN LA VARIABLE GLOBAL ssid EL NOMBRE DE LA
//  RED WIFI A LA QUE SE VA A CONECTAR
//
////////////////////////////////
//
void setSsid(String netid)
{
  int largo = 30;
  ssid = (char*)malloc(largo);
  netid.toCharArray(ssid,largo);
  //Serial.println("Esta es la ssid:");
  //Serial.println(ssid);
}

///////////////////////
//
//  METE EN LA VARIABLE GLOBAL password EL PASSWORD DE LA
//  RED WIFI A LA QUE SE VA A CONECTAR
//
////////////////////////////////
//
void setPwd(String netpwd)
{
  int largo = 30;
  password = (char*)malloc(largo);
  netpwd.toCharArray(password,largo);
  //Serial.println("Este es el password:");
  //Serial.println(password);
}
