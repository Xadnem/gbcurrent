/******************************************************************
 * 
 * MODULO PARA OPERACIONES CON EL BROKER Mqtt
 * 
 * 
 ******************************************************************/



#include <PubSubClient.h>
#include "WifiConnect.h"
#include "MqttConnect.h"

WiFiClient espClient;
PubSubClient client(espClient);
char* mqtt_server;
char* mqtt_user; 
char* mqtt_pwd;
char* mqtt_port; 
char* EstatTopic ;

const char* hiTopic = "placaOK";
const char* RestartTopic = "RestartESP";


////////////////////////////////
//
// Funció de callback a l'arribada d'un missatge
//
/////////////////////////////////
//
void incomingSub(char* topic, byte* payload, unsigned int length)
{
  Serial.print("Missatge rebut: [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  if(strcmp(topic,RestartTopic) == 0)
  {
    Serial.println("Rebooting!!");
    ESP.restart();
  }
}
// Fi callback()

///////////////////////////////
//
// ESTABLECE EL BROKER Y PUERTO DE CONEXION AL BROKER
// Y EL CALLBACK PARA EL MANEJO DE RECEPCION DE MENSAJES
//
//////////////////////////////////////
//
void setupMqtt()
{  
  // Concertir el char* del puerto en un int 
  int mqttport = atoi(mqtt_port);
  client.setServer(mqtt_server, mqttport);
  client.setCallback(incomingSub);
}


///////////////////////////
// 
// HACE UN INTENTOS DE CONEXION AL BROKER MQTT 
// SI NO LO CONSIGUE, INCREMENTA EL ULTIMO VALOR
// DE LA IP Y VUELVE A REALIZAR UN INTENTO HASTA LLEGAR 
// A 99 EN LOS DOS ULTIMOS DIGITOS DE LA IP DEL BROKER
//
/////////////////////////////////
//
void mqttConnect(void * parameter)
{  
  int cont = 0;
  while(true)
  {
    if(cont == 1000)
       Serial.println("Comprobando conexion a broker");
    /*Esperar hasta que se conecte el wifi */
    while((WiFi.status() != WL_CONNECTED) )
    {
      Serial.println("Esperando a wifi");
    }
    if (!client.connected()) {
      int contador = 0;
      int ipnum = 33;
      int intentos = 2;
      Serial.print("Intent de conexió MQTT...");
      // Creació d'un random client ID
      String clientId = "placaPorta";
      clientId += String(random(0xffff), HEX);
      while(!client.connected() && ipnum < 100)
      {       
        Serial.println("Intento de conexion con ip: ");
        Serial.println(mqtt_server);               
        if (client.connect(clientId.c_str(),mqtt_user,mqtt_pwd))
        {
          Serial.println("connectat");
          // Enviament d'un missatge de ACK de conexió OK al topic d'ACK
          client.publish(hiTopic, "¡Hola des de la placa!");
          client.subscribe(RestartTopic);
          vTaskDelay(10);  
          //vTaskDelete( NULL );
        }  
        contador++;
        if(contador == intentos)
        {     
          contador = 0;
          incrementaIP();
          setupMqtt();
          Serial.println("Nueva ip obtenida: ");
          Serial.println(mqtt_server);   
          ipnum++;        
          vTaskDelay(10); 
        }
      }
      if(ipnum >= 100)
      {
        // En caso de fallo en todos los intentos anteriores 
        Serial.print("fallada, rc=");
        Serial.print(client.state());
        Serial.println(" Nou intent dintre  d'uns segons");
        // Reiniciar valores
        mqtt_server = "192.168.1.33";
        contador = 0;
        ipnum = 33; 
      }
    }
    else
    {
      if(cont == 1000)
      {
        Serial.println("Conectado a broker");
        cont = 0;
      }
      cont ++;
    }
    vTaskDelay(10); 
  }
  vTaskDelete( NULL );
}



////////////////////////////
//
//  PUBLICA EL MENSAJE PASADO COMO SEGUNDO ARGUMENTO, EN EL TOPIC
//  PASADO COMO PRIMERO
//
/////////////////////////////////////////
//
void publicaMensaje(char * topic, char * msg)
{
  client.publish(topic, msg);
  client.loop();
}


////////////////////////////
//
//  INCREMENTA LOS DOS ULTIMOS DIGITOS DE LA IP ACTUAL DEL BROKER
//
///////////////////////////////
//
void incrementaIP()
{
  /* Obtener los dos ultimos digitos de la ip actual */
  char * lastdigits =(char*)malloc(3);
  lastdigits[0] = mqtt_server[10];
  lastdigits[1] = mqtt_server[11];
  lastdigits[2] = '\0';
  /* Convertir a int e incrementar */
  int newdigit = atoi(lastdigits);
  newdigit++;
  /* Reconstruir la ip con los nuevos digitos */
  sprintf(lastdigits,"%d",newdigit);
  mqtt_server[10] = lastdigits[0];
  mqtt_server[11] = lastdigits[1];
}

int estatConexio()
{
  return client.connected();
}

/* Setters para establecer los valores de las variables de conexion */

void setMqtt_server(String ip)
{
  int largo = 30;
  mqtt_server = (char*)malloc(largo);
  ip.toCharArray(mqtt_server,largo);
}

void setMqtt_port(String port)
{
  int largo = 30;
  mqtt_port = (char*)malloc(largo);
  port.toCharArray(mqtt_port,largo);
}

void setMqtt_user(String user)
{
  int largo = 30;
  mqtt_user = (char*)malloc(largo);
  user.toCharArray(mqtt_user,largo);
}

void setMqtt_pwd(String pwd)
{
  int largo = 30;
  mqtt_pwd = (char*)malloc(largo);
  pwd.toCharArray(mqtt_pwd,largo);
}

void setMqtt_topic(String topic)
{
  int largo = 30;
  EstatTopic = (char*)malloc(largo);
  topic.toCharArray(EstatTopic,largo);
}

/* Getter para el topic d'enviament del estat del boto */
char * getTopic()
{
  return EstatTopic;
}
